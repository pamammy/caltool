// var currhost = "http://localhost:8999/ktbe/th/";
// var currhosten = "http://localhost:8999/ktbe/en/";

var currhost = "http://10.9.152.43/th/";
var currhosten = "http://10.9.152.43/en/";

/*-------------------------- Load Header and Footer ------------------------------*/
function loadheaderfooter(tmppath,headnum){
  $.get(tmppath+"includes/header-th.html",function(data){
    var res = "";
    res = data.replace(new RegExp("../dist/","g"),"dist/");
    res = res.replace(new RegExp("dist/","g"),tmppath+"dist/");
    res = res.replace(new RegExp("/th/","g"),currhost);
    res = res.replace(new RegExp("/en/","g"),currhosten);
    $("#header").replaceWith(res);
    initglobal(headnum);
    bindmobilemenu();
  });

  $.get(tmppath+"includes/footer-th.html",function(data){
    var res = "";
    res = data.replace(new RegExp("../dist/","g"),"dist/");
    res = res.replace(new RegExp("dist/","g"),tmppath+"dist/");
    res = res.replace(new RegExp("/th/","g"),currhost);
    res = res.replace(new RegExp("/en/","g"),currhosten);
    $("#footer").replaceWith(res);
    initglobal(headnum);
  });

  $.get(tmppath+"includes/side-menu-th.html",function(data){
    var res = "";
    res = data.replace(new RegExp("../dist/","g"),"dist/");
    res = res.replace(new RegExp("dist/","g"),tmppath+"dist/");
    res = res.replace(new RegExp("/th/","g"),currhost);
    res = res.replace(new RegExp("/en/","g"),currhosten);
    $("#sidemenu").replaceWith(res);
    initglobal(headnum);
    initfaqblock();
  });

}

function loadheaderfooteren(tmppath,headnum){
  $.get(tmppath+"includes/header-en.html",function(data){
    var res = "";
    res = data.replace(new RegExp("../dist/","g"),"dist/");
    res = res.replace(new RegExp("dist/","g"),tmppath+"dist/");
    res = res.replace(new RegExp("/en/","g"),currhosten);
    res = res.replace(new RegExp("/th/","g"),currhost);
    $("#header").replaceWith(res);
    initglobal(headnum);
    bindmobilemenu();
  });

  $.get(tmppath+"includes/footer-en.html",function(data){
    var res = "";
    res = data.replace(new RegExp("../dist/","g"),"dist/");
    res = res.replace(new RegExp("dist/","g"),tmppath+"dist/");
    res = res.replace(new RegExp("/en/","g"),currhosten);
    res = res.replace(new RegExp("/th/","g"),currhost);
    $("#footer").replaceWith(res);
    initglobal(headnum);
  });
    $.get(tmppath+"includes/side-menu-en.html",function(data){
    var res = "";
    res = data.replace(new RegExp("../dist/","g"),"dist/");
    res = res.replace(new RegExp("dist/","g"),tmppath+"dist/");
    res = res.replace(new RegExp("/en/","g"),currhosten);
    res = res.replace(new RegExp("/th/","g"),currhost);
    $("#sidemenu").replaceWith(res);
    initglobal(headnum);
    initfaqblock();
  });
}

/*-------------------------------- function global init ---------------------*/
function initglobal(headnum){
    //bindmobilemenu();
    openheaderlv1(headnum);
    openheaderlv2();
    //initfaqblock();
    slideoutmenu();
    if($(window).width()<1324){
        $(".headerlv1.logo").width(145);
    }else{
        $(".headerlv1.logo").width(210);
    }
    
    $(window).resize(function(){
        if($(window).width()<1324){
            $(".headerlv1.logo").width(145);
        }else{
            $(".headerlv1.logo").width(210);
        }
    });
}

/*--------------------------------function for header ------------------------*/
var currentheaderlv1 = '';
var tempcurrenthead = '';
function openheaderlv1(objname){
    // if(objname!=currentheaderlv1){
        if(objname == 'set0'){
            objname = 'set1';
        }
        currentheaderlv1 = objname;
        $(".headerlv1.showmenu").removeClass("showmenu").addClass("hidemenu");

        $(".hfirstlink").removeClass("isselected");
        $(".hfirstlink."+objname).addClass("isselected");
        
        // check if change menu ,will fade out 
        if(tempcurrenthead!=currentheaderlv1){
            $(".headerlv1.hidemenu").fadeOut(500,function(){
            }).css("display","none");

            tempcurrenthead = currentheaderlv1;
        }

        $(".headerlv1."+objname).fadeIn(500,function(){

        }).css("display","inline-block");

    // }
}
/**/
var currentheaderlv2 = '';
var intervalrollout;
var counthover = 0;
function openheaderlv2(objname){

    if(currentheaderlv2!=objname){

        currentheaderlv2 = objname;
        $(".headerlv2").hide();
        $(".hsecondlink").removeClass("isselected");
        $(".hsecondlink."+objname).addClass("isselected");

        counttimerollout(objname);
        
        // $(".headerlv2."+objname).slideDown(500,function(){});
        if(counthover > 1){
            $(".headerlv2."+objname).show();
        }else{
            $(".headerlv2."+objname).fadeIn(200);
        }
        counthover += 1;

    }
    // else{
    //     callbackfrominterval();
    // }
    
}
function closeheaderlv2(){
    currentheaderlv2 = '';
    $(".hsecondlink").removeClass("isselected");
    // $(".headerlv2").slideUp(500,function(){

    // });
    $(".headerlv2").fadeOut(100);
    counthover = 1;
}

function callbackfrominterval(){
    clearInterval(intervalrollout);
    closeheaderlv2();
}

function counttimerollout(objname){

   /* var inmenu = true;

    $('.headerlv2.'+objname).mouseleave(function(){
        inmenu = false;
    });
    
    $('.headerlv2.'+objname).mousemove(function(){
        if(!inmenu){
            clearInterval(intervalrollout);
            intervalrollout = setInterval(callbackfrominterval,1400);
        }
        
    });*/

    
    $('.headerlv2.'+objname).mouseleave(function(){
        clearInterval(intervalrollout);
        intervalrollout = setInterval(callbackfrominterval,400);
    });

    
}

// function closeheaderlv2(objname){
//     currentheaderlv2 = '';
//     $(".hsecondlink").removeClass("isselected");
//     $(".headerlv2."+objname).slideUp(500,function(){
//     });
// }

// var curropenobj;

// function callbackfrominterval(){
   
//     $('.headerlv2.'+curropenobj).mouseleave(function(){
//         clearInterval(intervalrollout);
//         closeheaderlv2(curropenobj);
//     });
   
// }
// function counttimerollout(objname){
    
//     $('.headerlv2.'+objname).mousemove(function(){
//         clearInterval(intervalrollout);
//         curropenobj = objname;
//         intervalrollout = setInterval(callbackfrominterval,1400);
//     });
// }
/**/


function opendivlang(){
    $(".divclicklang").toggleClass("selected");
    if($(".divclicklang").hasClass("selected")){
        $(".choiceoptions").slideDown(300,function(){});
    }else{
        $(".choiceoptions").slideUp(300,function(){});
    }
    
}

/*------------------------------- function for footer ----------------------*/
function openfooter(idx){
    
  if($(window).width()<992){

    if($('.footerlv1:eq('+idx+') > li > a').hasClass("isselected")){
      $('.footerlv1:eq('+idx+') > li > a').removeClass("isselected");
    }else{
      $('.footerlv1:eq('+idx+') > li > a').addClass("isselected");
    }
    
    $('.footerlv2:eq('+idx+')').slideToggle("fast",function(){
        /*----- For IE8 -----*/
        if($.browser.msie && parseFloat($.browser.version)<=8){
            $('.footerlv1:eq('+idx+') > li > a').toggleClass("footermbclose");
        }
    });

  }
}

$(window).resize(function(){
  $('.footerlv1 > li > a').removeClass("isselected");
  if($(window).width()<992){
    /*----- For IE8 -----*/
    if($.browser.msie && parseFloat($.browser.version)<=8){
        $('.footerlv1 > li > a i').hide();
        $('.footerlv1 > li > a').addClass('footermbopen');
    }else{
        $('.footerlv2').css("display","none");
    }
    
  }else{    
    $('.footerlv2').show();
  }
});

/*for mobile */
function bindmobilemenu(){

    /*----- For IE8 -----*/
    if($.browser.msie && parseFloat($.browser.version)<=8){
        $('#menu-mobile-button').addClass('headermbopen');
    }

    $('#menu-mobile-button').click(function(){
        /*----- For IE8 -----*/
        if($.browser.msie && parseFloat($.browser.version)<=8){
            $('#menu-mobile-button').toggleClass('headermbclose');
        }

        $(this).toggleClass('open');
        if($(this).hasClass('open')){
            $(".secondmenumb").fadeIn(350,function(){});
            $(".thirdmenumb").slideDown(300,function(){});
        }else{
            $('.divlanguagemb').removeClass('open');
            $(".divlanguagechoice").slideUp(200,function(){});
            $(".divlvsubmain").hide();
            $('.lv1mblink').removeClass('selected');
            $('.lv2').slideUp(200,function(){});
            $(".thirdmenumb").slideUp(200,function(){});
            $(".secondmenumb").fadeOut(100,function(){});
            $(".thirdmenumb").css({left:"0%"});
            $(".fourthmenumb").css({left:"100%"});
        }
    });
    $('.divlanguagemb').click(function(){
        $(this).toggleClass('open');
        if($(this).hasClass('open')){
            $(".divlanguagechoice").slideDown(200,function(){});
        }else{
            $(".divlanguagechoice").slideUp(200,function(){});
        }
    });
    
}
function openmobilesubmenu(objname){
    $(".divlvsubmain").hide();
    $("."+objname).show();
    $(".thirdmenumb").animate({left:"-100%"},500,function(){});
    $(".fourthmenumb").animate({left:"0%"},500,function(){});
}
function backmobilesubmenu(){
    $(".thirdmenumb").animate({left:"0%"},500,function(){});
    $(".fourthmenumb").animate({left:"100%"},500,function(){});
    $('.lv1mblink').removeClass('selected');
    $('.lv2').slideUp(200,function(){});
}

var subsetlv2 = '';
var numopen = 0;
function openmobilelv2menu(objname){
    $('.lv1mblink').removeClass('selected');
    $('.lv2').slideUp(200,function(){});

    if(subsetlv2 != objname){
        $('.'+objname+'link').addClass('selected');
        $('.'+objname).slideDown(200,function(){});
        subsetlv2 = objname;
        numopen = 0; //set start 0 again
    }else{
        if(numopen < 1){
            numopen = numopen+1; 
        }else{
            $('.'+objname+'link').addClass('selected');
            $('.'+objname).slideDown(200,function(){});
            numopen = 0;
        }
    }    
}
/*------------------------------- function for side menu --------------------------*/
function slideoutmenu(){
    $('.sidemenu').hover(function(){
        $('.tabs-menu').stop(true,false).animate({
            right:'40px'
        },800);
    },function(){
        $('.tabs-menu').stop(true,false).animate({
            right:'-180px'
        },800);
    });
}


/*------------------------------- function for Share button ----------------------*/
function sharepage(objname){
  
  $('#'+objname).toggleClass('active');

  var socialwidth = $('#'+objname).parent().parent().find(".followsocial").width();

  if($('#'+objname).hasClass('active')){
    $('#'+objname).parent().parent().find('.share-palette').animate({
        marginLeft: '-='+(socialwidth+5)+'px',
        width:socialwidth+'px'
    },500);
    
  }else{
    $('#'+objname).parent().parent().find('.share-palette').animate({
        marginLeft: '0px',
        width: '0px'
    },500);
  }
}
/*--------------------- Match height for all columns ----------------------*/
function matchheight(){
    var maxheight = 0;
    $(".contentlist > div").css("height","auto");
    $(".contentlist > div").each(function(index){
        //console.log($(".contentlist > div:eq("+index+")").height());
        if($(".contentlist > div:eq("+index+")").height()>maxheight){
            maxheight = $(".contentlist > div:eq("+index+")").height();
        }
    });
    $(".contentlist > div").height(maxheight);
    $(window).resize(function(){
        maxheight = 0;
        $(".contentlist > div").css("height","auto");
        $(".contentlist > div").each(function(index){
            //console.log($(".contentlist > div:eq("+index+")").height());
            if($(".contentlist > div:eq("+index+")").height()>maxheight){
                maxheight = $(".contentlist > div:eq("+index+")").height();
            }
        });
        $(".contentlist > div").height(maxheight);
    });
}
/**/
function matchheight2(){

    // var maxheightsm = 0;
    // var maxheightlg = 0;

    // $(".contentlist > div.itmlg").css("height","auto");
    // $(".contentlist > div.itmsm").css("height","auto");

    
    // $(".contentlist > div.itmlg").height(maxheightlg);

    // $(".contentlist > div.itmsm").each(function(index){
    //     if($(".contentlist > div.itmsm:eq("+index+")").height()>maxheightsm){
    //         maxheightsm = $(".contentlist > div.itmsm:eq("+index+")").height();
    //     }
    // });
    // $(".contentlist > div.itmsm").height(maxheightsm);

    // $(window).resize(function(){
    //      maxheightsm = 0;
    //      maxheightlg = 0;

    //     $(".contentlist > div.itmlg").css("height","auto");
    //     $(".contentlist > div.itmsm").css("height","auto");

       
    //     $(".contentlist > div.itmlg").height(maxheightlg);

    //     $(".contentlist > div.itmsm").each(function(index){
    //         if($(".contentlist > div.itmsm:eq("+index+")").height()>maxheightsm){
    //             maxheightsm = $(".contentlist > div.itmsm:eq("+index+")").height();
    //         }
    //     });
    //     $(".contentlist > div.itmsm").height(maxheightsm);
    // });


}

/*------------------------------- for Slick template ----------------------*/
 $(function(){

    moveslicktype2(); /*custom navigator for slickblock2*/ 

    if(parseFloat($.browser.version) > 8){
        $('.slickblock1').slick({
            dots:true,
            autoplay: true,
            autoplaySpeed: 3000,
            infinite:true,
            arrows:false,
            swipe:true,
            swipeToSlide:true
        });
    }else{
        $('.slickblock1').slick({
            dots:true,
            autoplay: false,
            infinite:true,
            arrows:false,
            swipe:true,
            swipeToSlide:true
        });
    }

    $('.slickblock2').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:4,
        slidesToScroll:4,
        responsive:
        [{
            breakpoint:1200,
            settings:{
                slidesToShow:3,
                slidesToScroll:3
            }
        },{
            breakpoint:992,
            settings:{
                slidesToShow:2,
                slidesToScroll:2
            }

        },{
            breakpoint:768,
            settings:{
                slidesToShow:2.5,
                slidesToScroll:2
            }

        },]
    });

    $('.slickblock3').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        initialSlide: 0,
        slidesToShow:2.5,
        slidesToScroll:2,
        variableWidth: true,
        adaptiveHeight: true,
        responsive:
        [{
            breakpoint:768,
            settings:{
                slidesToShow:1,
                slidesToScroll:1,
                initialSlide: 0,
                variableWidth: false,
                adaptiveHeight: false,
            }

        }]
        
    }); 


    $('.slickblock4').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:3,
        slidesToScroll:3,
        responsive:
        [{
            breakpoint:992,
            settings:{
                slidesToShow:2,
                slidesToScroll:2
            }

        },{
            breakpoint:768,
            settings:{
                slidesToShow:1.3,
                slidesToScroll:1
            }
        }]
    });
    $('.slickblock5').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:4,
        slidesToScroll:4,
        responsive:
        [{
            breakpoint:768,
            settings:{
                slidesToShow:2.5,
                slidesToScroll:2,
                swipe:true,
            }
        },{
            breakpoint:480,
            settings:{
                slidesToShow:2.4,
                slidesToScroll:2,
                swipe:true,
            }
        }]
    });
    $('.slickblock6').slick({
        dots:true,
        infinite:true,
        arrows:false,
        swipe:true,
        slidesToShow:3,
        responsive:
        [{
            breakpoint:768,
            settings:{
                slidesToShow:2,
                slidesToScroll:2,
                swipe:true,
            }
        }]
    });
    $('.slickblock7').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        responsive: 
        [{
            breakpoint: 768,
            settings:{
                infinite: false,
                slidesToShow: 1.5,
                slidesToScroll: 1,
                dots: false
            }
        },
        {
            breakpoint: 480,
            settings:{
                infinite: false,
                slidesToShow: 1.2,
                slidesToScroll: 1,
                dots: false
            }
        }
        ]
        
    });
    $('.slickblock8').slick({
        dots:false,
        infinite:false,
        arrows:false,
        swipe:false,
        slidesToShow:3,
        responsive:
        [{
            breakpoint:768,
            settings:{
                infinite:false,
                slidesToShow:1.5,
                slidesToScroll:1,
                swipe:true,
            }
        },]
    });
     $('.slickblock9').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:1,
    });  

    $('.slickblock10-preview').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slickblock10-nav'
    });

    $('.slickblock10-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slickblock10-preview',
        focusOnSelect: true,
        centerMode: true,
        responsive:
        [{
            breakpoint:768,
            settings:{
                slidesToShow:3
            }
        },
        {
            breakpoint:480,
            settings:{
                slidesToShow:2
            }
        }]
    });
    $('.slickblock11').slick({
        infinite: false,
        slidesToShow: 7,
        slidesToScroll: 7,
        arrows: false,
        responsive: 
            [{
                breakpoint:1201,
                settings:{
                    infinite: true,
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint:993,
                settings:{
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }]
    });
     $('.slickblock12').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:3,
        slidesToScroll:3,
        responsive:
        [{
            breakpoint:992,
            settings:{
                slidesToShow:2,
                slidesToScroll:2
            }

        },{
            breakpoint:768,
            settings:{
                slidesToShow:1.3,
                slidesToScroll:1
            }
        }]
    });
    $('.slickblock13').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:3,
        slidesToScroll:3,
        responsive:
        [{
            breakpoint:992,
            settings:{
                slidesToShow:2,
                slidesToScroll:2
            }

        },{
            breakpoint:768,
            settings:{
                slidesToShow:1.3,
                slidesToScroll:1
            }
        }]
    });
    $('.slickblock14').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:3,
        slidesToScroll:3,
        responsive:
        [{
            breakpoint:992,
            settings:{
                slidesToShow:2,
                slidesToScroll:2
            }

        },{
            breakpoint:768,
            settings:{
                slidesToShow:1.3,
                slidesToScroll:1
            }
        }]
    });

    $('.slickblock15').slick({
        dots:false,
        infinite:false,
        arrows:false,
        swipe:false,
        slidesToShow:4,
        responsive:
        [{
            breakpoint:992,
            settings:{
                swipe:true,
                slidesToShow:3,
                slidesToScroll:3
            }
        },{
            breakpoint:768,
            settings:{
                swipe:true,
                slidesToShow:1.3,
                slidesToScroll:1
            }
        }]
    });
    $('.slickblock16').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:1,
        slidesToScroll:1
    });
     $('.slickblock17').slick({
        dots:false,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:3,
        slidesToScroll:3,
        responsive:
        [{
            breakpoint:992,
            settings:{
                slidesToShow:2,
                slidesToScroll:2
            }

        },{
            breakpoint:768,
            settings:{
                slidesToShow:1.3,
                slidesToScroll:1
            }
        }]
    });

    $('.slickblock18').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        initialSlide: 0,
        slidesToShow:2.5,
        slidesToScroll:2,
        variableWidth: true,
        adaptiveHeight: true,
        responsive:
        [{
            breakpoint:768,
            settings:{
                slidesToShow:1,
                slidesToScroll:1,
                initialSlide: 0,
                variableWidth: false,
                adaptiveHeight: false,
            }

        }]
        
    }); 

    $('.slickblock19').slick({
        dots:false,
        infinite:true,
        arrows:false,
        slidesToShow:3,
        slidesToScroll:1,
        swipe:true,
        swipeToSlide:true,
        centerMode: true,
        centerPadding: '0px',
        adaptiveHeight: true,
        useTransform: true,
        responsive:
        [{
            breakpoint:768,
            settings:{
                slidesToShow:1,
                slidesToScroll:1,
                centerMode: false,
                adaptiveHeight: false,
                useTransform: false
            }
        }]
    });

    $('.slickblock20').slick({
        dots:true,
        infinite:false,
        arrows:false,
        swipe:true,
        slidesToShow:4,
        slidesToScroll:4
    });


    $('.slickblock9').on('beforeChange',function(){
        openpic();
    });

    $('.slickblock9').on('afterChange',function(){
        opacpic();
    });


    function opacpic(){
        var beforeitem = -1;
        $(".slickblock9 .item").each(function(index){
            if($(".slickblock9 .item:eq("+index+")").attr("aria-hidden")=="false"){
                beforeitem = index-1;
            }
        });
        $(".slickblock9 .item").removeClass("opcpic");
        $(".slickblock9 .item:eq("+beforeitem+")").addClass("opcpic");
    }

    function openpic(){
        var beforeitem = -1;
        $(".slickblock9 .item").each(function(index){
            if($(".slickblock9 .item:eq("+index+")").attr("aria-hidden")=="false"){
                beforeitem = index-1;
            }
        });
        $(".slickblock9 .item:eq("+beforeitem+")").removeClass("opcpic");
    }


    $(function(){
        opacpic();
        openpic();
    });

    $('.slickblock11').on('afterChange',function(){
        delvline();
    });
    function delvline(){
        var lastitem = -1;
        var firstitem = 0;
        var countitem = 0;
        $(".slickblock11 .item").each(function(index){
            if($(".slickblock11 .item:eq("+index+")").attr("aria-hidden")=="false"){
                countitem += 1;
                if(countitem == 1){
                    firstitem = index-1;
                }
                lastitem = index;
            }
        });
        $(".slickblock11 .item").removeClass("vline");
        $(".slickblock11 .item").addClass("vline");
        $(".slickblock11 .item:eq("+lastitem+")").removeClass("vline");
        $(".slickblock11 .item:eq("+firstitem+")").removeClass("vline");
    }
    $(function(){
        delvline();
    });


    function moveslicktype2(){
        $('.prev-slide').on('click',function(){
            $('.slickblock2').slick('slickPrev');
        });
        $('.next-slide').on('click',function(){
            $('.slickblock2').slick('slickNext');
        });

        $('.prev-slide10').on('click',function(){
                $('.slickblock10-preview').slick('slickPrev');
        });
        $('.next-slide10').on('click',function(){
                $('.slickblock10-preview').slick('slickNext');
        });

        $('.prev-slide11').on('click',function(){
            $('.slickblock11').slick('slickPrev');
        });
        $('.next-slide11').on('click',function(){
            $('.slickblock11').slick('slickNext');
        });

        $('.prev-slide16').on('click',function(){
            $('.slickblock16').slick('slickPrev');
        });
        $('.next-slide16').on('click',function(){
            $('.slickblock16').slick('slickNext');
        });

        $('.prev-slide19').on('click',function(){
            $('.slickblock19').slick('slickPrev');
        });
        $('.next-slide19').on('click',function(){
            $('.slickblock19').slick('slickNext');
        });

        $('.prev-slide20').on('click',function(){
            $('.slickblock20').slick('slickPrev');
        });
        $('.next-slide20').on('click',function(){
            $('.slickblock20').slick('slickNext');
        });
    }

});


/*----------------------------------------for slickblock3 , slickblock18 , and slickblock19 (getmaxheight2 for slickblock19)  ----------------------------------------------*/ 
var tmphigh = 0;
var currentactive = false;
var isIE8 = false;
function getmaxheight(){
    tmphigh = 0;
    if($(window).width() > 767){
        $(".picframe3").each(function(index){
            if($(this).height() > tmphigh){
                tmphigh = $(this).height();
            }
        });
        $(".picframe3").height(tmphigh);
        if(isIE8){
            setTimeout(function(){
                currentactive = false;
            },2000);
        }
    }
}

function getmaxheight2(){
    tmphigh = 0;
    if($(window).width() > 767){
        tmphigh = $(".slick-center").height();
        tmphigh = tmphigh;
        $(".boxwrap").height(tmphigh);
        $(".slick-center").css('height','auto');
    }else{
        $(".boxwrap").height('auto');
    }
}

function resizeSlickItem(chkdivnm){
    var slkwidth = $(chkdivnm).width();
    if($(window).width() > 767){
        $( chkdivnm + " .item" ).each(function( index ) {
            if($(window).width() <=  parseInt($(chkdivnm).attr("data-resp-mobile-size"))){      
            
            $(this).width(slkwidth);				
        }else{
        
            $(this).width(parseInt($(this).attr("data-width-per"))*slkwidth/100);            
            }
        });
    }
}

 $(document).on('ready', function() {

    $(".picframe3").css('height','auto');
    $(".boxwrap").css('height','auto');
    if($(window).width() > 767){
        resizeSlickItem(".slickblock3");
        resizeSlickItem(".slickblock18");     
        getmaxheight();  
        getmaxheight2();  
    }  
 });

 $(window).resize(function(){
    /*----- For IE8 -----*/
    if($.browser.msie && parseFloat($.browser.version)<=8){
        isIE8 = true;
    }


    if(!currentactive){
        if(isIE8){
            currentactive = true;
        }

        $('.slickblock3 li:eq(0) button').click();
        $('.slickblock18 li:eq(0) button').click(); 
        tmphigh = 0;
        setTimeout(function(){
            $(".picframe3").css('height','auto');
            $(".boxwrap").css('height','auto');
            if($(window).width() > 767){
                resizeSlickItem(".slickblock3");   
                resizeSlickItem(".slickblock18");
                getmaxheight();
                getmaxheight2();
            }else{
                $(".boxwrap").height('auto');
            }
        },100);
    }
   
});


/*-----------------------------------------------------------------------------------------*/
function initfaqblock(){
    $('.mainfaqinc .questionclick').on('click',function(){
        $(this).toggleClass('active');
        $(this).parent().find('.catanswer').slideToggle(300,function(){});
    })
}
/*-----------------------------------chagne dropdown currency-----------------------------*/
function changecurrency(){
    var curItem = $('.dropdown-menu li');
    curItem.on('click',function(){
    if($('.dfVal').length==0){
            $('.dropdown-menu').prepend($('<li class="dfVal"><a href="#">ทุกสกุลเงิน</a></li>'));
            $('.dfVal').on('click',function(){
                $('#defaultVal').html("ทุกสกุลเงิน");
                $(this).remove();
                $('#currencyVal').val("ทุกสกุลเงิน");
                //console.log($('#currencyVal').val());
            });
        }
        var curVal = $(this).text();
        $('#currencyVal').val(curVal);
        var htmlStr = $(this).children().html();
        $('#defaultVal').html(htmlStr);
        //console.log($('#currencyVal').val());
    });
}

/*------------------------------- Close Button Lity --------------------------*/
function closelitybtn(){
    $('.lity-close').show();
    $('.lity-close').html('');
}

//-------------------------- new script --------------------------//
//-------------------------- slick template --------------------------//

// ------------------- for slickrev12, slickrev13 ---------------------//
var slickblockrev13_setting = {
    arrows:false,
    dots:true,
    infinite:false,
    responsive:[
        {
            breakpoint:767,
            slidesToShow:1,
            swipe:true,
        },
    ],
}
var slickblockrev12_setting = {
    slidesToShow:6,
    infinite:false,
    arrows:true,
    dots:false,
    slidesToScroll: 3,
    touchThreshold: 50,
    responsive:[
        {
            breakpoint:991,
            settings:{
                slidesToShow: 4,
            }
        },
        {
            breakpoint:767,
            settings: "unslick",
        },
    ],
}
// ------------------- end for slickrev12, slickrev13 ---------------------//

// ------------------- for unslick 2 device ---------------------//
var windowOld;
var windowUpdate;

$(document).ready(function(){
    windowUpdate = $(window).width();
    if($(window).width()>767){
        slickblockrev12();
        windowOld = 768;
    }else{
        slickblockrev13();
        windowOld = 767;
    }
});
$(window).resize(function(){
    windowUpdate = $(window).width();
    //unslick desktop
    if( windowOld > 767 && windowUpdate <= 767 && !$('.slickblockrev13').hasClass('slick-initialized')){
        $('.slickblockrev13').slick(slickblockrev13_setting);
    }else if(windowUpdate > 767 && $('.slickblockrev13').hasClass('slick-initialized')){
        //window>767
        setTimeout(function() {
            $('.slickblockrev13').slick('unslick');
        }, 500);
        
    }
    //unslick mobile
    if( windowOld <=767 && windowUpdate > 767){
        //window>767
        $('.slickblockrev12').slick(slickblockrev12_setting);
        
    }else if( windowUpdate <= 767 && $('.slickblockrev12').hasClass('slick-initialized')){
        $('.slickblockrev12').slick('unslick');
    }
    windowOld = windowUpdate;
});
// ------------------- end for unslick 2 device ---------------------//

 $(function(){
    slickblockrev1();
    slickblockrev2();
    slickblockrev3();
    slickblockrev4();
    slickblockrev5();
    slickblockrev6();
    slickblockrev7();
    slickblockrev8();
    slickblockrev9();
    slickblockrev10();
    slickblockrev11();
    slickblockrev14();
    detectFirefox();
 });
function slickblockrev1(){
    $('.slickblockrev1').slick({
        slidesToShow: 1,
        dots: true,
        fade: true,
        speed: 2000,
        touchThreshold: 100,
        waitForAnimate: true,
        // autoplay: true,
        // autoplaySpeed: 7000,
        // pauseOnFocus: false, 
        // pauseOnHover: false
    });
    $(".slickblockrev1 .slick-dots").wrap("<div class=\"slick-dots-wrap\"></div>");
    // $('.slickblockrev1 .background-chevron, .slickblockrev1 .chevron-overlay-default,.slickblockrev1 .fadeInDown').addClass('animated');
    setTimeout(function(){
        
        /*clear initial*/
        $('.slickblockrev1 .background-chevron, .slickblockrev1 .chevron-overlay-default').removeClass('fadeInDownImg fadeIn animated');
    }, 2000);
    $('.slickblockrev1').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        
        var currentElm = '.slickblockrev1 .item[data-slick-index='+currentSlide+'] ';
        var nextElm = '.slickblockrev1 .item[data-slick-index='+nextSlide+'] ';
        var Elm = '.slickblockrev1 ';
        
        // setTimeout(function(){
            $(currentElm+'.textanimate .hdtxt').removeClass('fadeInDown animated').addClass('fadeOutDown animated');
        
            $(currentElm+'.textanimate .text').removeClass('fadeInDown animated').addClass('fadeOutDown animated');
            $(currentElm+'.textanimate .buttonblock').removeClass('fadeInDown animated').addClass('fadeOutDown animated');
            $(currentElm+'.picblock1.hidden-xs').removeClass('fadeInDownImg animated').addClass('fadeOutDownImg animated');
        // }, 50);
        
        setTimeout(function(){
            /*clear old class*/
            $('.slickblockrev1 .chevron-overlay-default').removeClass('chevron-overlay-enter chevron-overlay-leave');
        }, 80);
        setTimeout(function(){
            $(Elm+'.chevron-overlay-default').addClass('chevron-overlay-enter');
            $(currentElm+'.chevron-overlay-default').on('animationend webkitAnimationEnd oAnimationEnd', function () {
                $(Elm+'.chevron-overlay-default').delay('100').addClass('chevron-overlay-leave');
            });
            
            $(nextElm+'.textanimate .hdtxt').removeClass('fadeOutDown animated').addClass('fadeInDown animated');
            $(nextElm+'.textanimate .text').removeClass('fadeOutDown animated').addClass('fadeInDown animated');
            $(nextElm+'.textanimate .buttonblock').removeClass('fadeOutDown animated').addClass('fadeInDown animated');
            $(nextElm+'.picblock1.hidden-xs').removeClass('fadeOutDownImg animated').addClass('fadeInDownImg animated');
        }, 200);
        
        
    });
}
function slickblockrev2(){
    $('.slickblockrev2').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        touchThreshold: 50,
        dots: true,
        infinite: false,
        arrows: true,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
    });
    setTimeout(function(){
        /*clear initial*/
        $('.slickblockrev1 .background-chevron, .slickblockrev1 .chevron-overlay-default').removeClass('fadeInDownImg fadeIn animated');
    }, 2000);
    /* nav */
    // $('.prev-slide-2').on('click',function(){
    //     $('.slickblockrev2').slick('slickPrev');
    // });
    // $('.next-slide-2').on('click',function(){
    //     $('.slickblockrev2').slick('slickNext');
    // });
}
function slickblockrev3(){
    $('.slickblockrev3').slick({
        initialSlide: 0,
        slidesToShow: 3,
        slidesToScroll:2,
        adaptiveHeight: true,

        touchThreshold: 50,
        dots: true,
        infinite: false,
        arrows: true,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
        responsive:
        [{
            breakpoint:767,
            settings:{
                slidesToShow:1,
                slidesToScroll:1,
                // infinite: true,
                swipe:true,
                centerMode: true,
                adaptiveHeight: false,
            }
        },]
    });

    // $('.slickblockrev3').slick('slickGoTo', 1, true);
}
function slickblockrev4(){
    $('.slickblockrev4').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        touchThreshold: 50,
        dots: true,
        arrows: true,
        infinite: false,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
        responsive:
        [   
            {
                breakpoint:1200,
                settings:{
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    swipe:true,
                },
            },
            {
                breakpoint:767,
                settings:{
                    slidesToShow: 1.5,
                    slidesToScroll: 1,
                    swipe:true,
                },
            },
        ]
    });
}
function slickblockrev5(){
    $('.slickblockrev5').slick({
        slidesToShow: 3.5,
        slidesToScroll: 3,
        dots: true,
        arrows: false,
        infinite: false,
        touchThreshold: 50,
        responsive:
        [
            {
                breakpoint:992,
                settings:{
                    slidesToShow: 2.5,
                    slidesToScroll: 1,
                    swipe:true,
                }
            },
            {
            breakpoint:767,
            settings:{
                slidesToShow: 1.2,
                slidesToScroll: 1,
                swipe:true,
            }
        },]
    });
}
function slickblockrev6(){
    var slickblockrev6 = $('.slickblockrev6');
    //initial background images
    if($(window).width()>767){
        var initialsrc = $('.slickblockrev6 .picblock1.hidden-xs img').attr('src');
    }else{
        var initialsrc = $('.slickblockrev6 .picblock1.visible-xs img').attr('src');
    }

    $('.slickblockrev6_background').css('background-image', 'url(' + initialsrc + ')');
    
    slickblockrev6.on('init', function(event, slick, currentSlide) {
        var
        cur = $(slick.$slides[slick.currentSlide]),
        next = cur.next(),
        prev = cur.prev();
        prev.addClass('slick-sprev');
        next.addClass('slick-snext');
        cur.removeClass('slick-snext').removeClass('slick-sprev');
        slick.$prev = prev;
        slick.$next = next;
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        // console.log('beforeChange');
        var
        cur = $(slick.$slides[nextSlide]);
        // console.log(slick.$prev, slick.$next);
        slick.$prev.removeClass('slick-sprev');
        slick.$next.removeClass('slick-snext');
        next = cur.next(),
        prev = cur.prev();
        prev.prev();
        prev.next();
        prev.addClass('slick-sprev');
        next.addClass('slick-snext');
        slick.$prev = prev;
        slick.$next = next;
        cur.removeClass('slick-next').removeClass('slick-sprev');

        //change background after swipe
        var nextElm = '.slickblockrev6 .item[data-slick-index='+nextSlide+'] ';
        if($(window).width()>767){
            var initialsrc = $(nextElm+' .picblock1.hidden-xs img').attr('src');
        }else{
            var initialsrc = $(nextElm+' .picblock1.visible-xs img').attr('src');
        }
        $('.slickblockrev6_background').css('background-image', 'url(' + initialsrc + ')');
    });
    
    slickblockrev6.slick({
        speed: 1000,
        // arrows: true,
        dots: true,
        focusOnSelect: true,
        // prevArrow: '<button> prev</button>',
        // nextArrow: '<button> next</button>',
        infinite: true,
        centerMode: true,
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerPadding: '0',
        swipe: true,
        autoplay: true,
        autoplaySpeed: 6000,
        pauseOnFocus: false, 
        pauseOnHover: false,
        /*infinite: false,*/
    });
}
function slickblockrev7(){
    $('.slickblockrev7').slick({
        slidesToShow: 3,
        slidesToScroll: 2,
        touchThreshold: 50,
        dots: true,
        arrows: true,
        infinite: false,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
        // lazyLoad: 'ondemand',
        responsive:
        [   
            {
                breakpoint:1200,
                settings:{
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    swipe:true,
                },
            },
            {
                breakpoint:992,
                settings:{
                    slidesToShow: 2.5,
                    slidesToScroll: 1,
                    swipe:true,
                },
            },
            {
                breakpoint:600,
                settings:{
                    slidesToShow: 1.1,
                    slidesToScroll: 1,
                    swipe:true,
                },
            },
        ]
    });
}
function slickblockrev8(){
    $('.slickblockrev8').slick({
        slidesToShow: 1,
        dots: true,
        speed: 1000,
        waitForAnimate: true,
        touchThreshold: 20,
        fade: true,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnFocus: false, 
        pauseOnHover: false
    });
    $(".slickblockrev8 .slick-dots").wrap("<div class=\"slick-dots-wrap\"></div>");

    $('.slickblockrev8').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var currentElm = '.slickblockrev8 .item[data-slick-index='+currentSlide+'] ';
        var nextElm = '.slickblockrev8 .item[data-slick-index='+nextSlide+'] ';
        $(currentElm+'.blocktitle .hdtxt').removeClass('fadeInDown animated').addClass('fadeOutDown animated');
        $(currentElm+'.blocktitle .text').removeClass('fadeInDown animated').addClass('fadeOutDown animated');
        $(currentElm+'.blocktitle .buttonblock').removeClass('fadeInDown animated').addClass('fadeOutDown animated');
        $(currentElm+'.blocktitle .btncircle-arrow').removeClass('fadeIn animated').addClass('fadeOut animated');
        
        $(nextElm+'.blocktitle .hdtxt').removeClass('fadeOutDown animated').addClass('fadeInDown animated');
        $(nextElm+'.blocktitle .text').removeClass('fadeOutDown animated').addClass('fadeInDown animated');
        $(nextElm+'.blocktitle .buttonblock').removeClass('fadeOutDown animated').addClass('fadeInDown animated');
        $(nextElm+'.blocktitle .btncircle-arrow').removeClass('fadeOut animated').addClass('fadeIn animated');

    });
}
function slickblockrev9(){
    $('.slickblockrev9').slick({
        slidesToShow: 1,
        dots: true,
        touchThreshold: 20,
        fade: true,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
    });
}
function slickblockrev10(){
    $('.slickblockrev10').slick({
        slidesToShow: 1,
        arrows: true,
        dots: true,
        touchThreshold: 20,
        fade: true,
        autoplay: true,
        autoplaySpeed: 4500,
        pauseOnFocus: false, 
        pauseOnHover: false,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
    });
}

function slickblockrev11(){
    $('.slickblockrev11').slick({
        slidesToShow: 2.5,
        slidesToScroll: 2,
        touchThreshold: 50,
        dots: true,
        arrows: false,
        infinite: false,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
        responsive:
        [   
          
            {
                breakpoint:991,
                settings:{
                    slidesToShow: 1.5,
                    slidesToScroll: 1,
                    swipe:true,
                },
            },
        ]
    });
}
function slickblockrev12(){
    $('.slickblockrev12').slick({
        slidesToShow:6,
        infinite:false,
        arrows:true,
        dots:false,
        slidesToScroll: 3,
        touchThreshold: 50,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
        responsive:[
            {
                breakpoint:991,
                settings:{
                    slidesToShow: 5,
                }
            },
            {
                breakpoint:767,
                settings: "unslick",
            },
        ],
    });
    
}
function slickblockrev13(){
    $('.slickblockrev13').slick({
        arrows:false,
        dots:true,
        infinite:false,
        touchThreshold: 50,
        responsive:[
            {
                breakpoint:767,
                slidesToShow:1,
                
                
                swipe:true,
            },
        ],
    });
    
}

function slickblockrev14(){
    $('.slickblockrev14').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        touchThreshold: 50,
        dots: true,
        arrows: true,
        infinite: false,
        prevArrow: '<a class="prev-slide"></a>',
        nextArrow: '<a class="next-slide"></a>',
        responsive:
        [   
            
            {
                breakpoint:767,
                settings:{
                    slidesToShow: 2.5,
                    swipe:true,
                },
            },
            {
                breakpoint:480,
                settings:{
                    slidesToShow: 1.2,
                    swipe:true,
                },
            },
        ]
    });
    
}
function detectFirefox(){
    if($('.slickblockrev6_wrapper').length && navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
        // Do Firefox-related activities
        $('.slickblockrev6_wrapper').find('svg').removeAttr('style');
        $('.slickblockrev6_wrapper').find('svg').css({
            position:'absolute',
            height:0,
        })
   }
}
/* backPage */
function backPage() {
    if (document.referrer.indexOf(window.location.host) !== -1) {
        window.history.back();
    } else {
        //ถ้าเข้าเพจโดยไม่ได้กดจากในเว็บ ให้ redirec
        window.location.replace('/');
    }
}
/* backPage */
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gdmFyIGN1cnJob3N0ID0gXCJodHRwOi8vbG9jYWxob3N0Ojg5OTkva3RiZS90aC9cIjtcclxuLy8gdmFyIGN1cnJob3N0ZW4gPSBcImh0dHA6Ly9sb2NhbGhvc3Q6ODk5OS9rdGJlL2VuL1wiO1xyXG5cclxudmFyIGN1cnJob3N0ID0gXCJodHRwOi8vMTAuOS4xNTIuNDMvdGgvXCI7XHJcbnZhciBjdXJyaG9zdGVuID0gXCJodHRwOi8vMTAuOS4xNTIuNDMvZW4vXCI7XHJcblxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIExvYWQgSGVhZGVyIGFuZCBGb290ZXIgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuZnVuY3Rpb24gbG9hZGhlYWRlcmZvb3Rlcih0bXBwYXRoLGhlYWRudW0pe1xyXG4gICQuZ2V0KHRtcHBhdGgrXCJpbmNsdWRlcy9oZWFkZXItdGguaHRtbFwiLGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgdmFyIHJlcyA9IFwiXCI7XHJcbiAgICByZXMgPSBkYXRhLnJlcGxhY2UobmV3IFJlZ0V4cChcIi4uL2Rpc3QvXCIsXCJnXCIpLFwiZGlzdC9cIik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiZGlzdC9cIixcImdcIiksdG1wcGF0aCtcImRpc3QvXCIpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi90aC9cIixcImdcIiksY3Vycmhvc3QpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi9lbi9cIixcImdcIiksY3Vycmhvc3Rlbik7XHJcbiAgICAkKFwiI2hlYWRlclwiKS5yZXBsYWNlV2l0aChyZXMpO1xyXG4gICAgaW5pdGdsb2JhbChoZWFkbnVtKTtcclxuICAgIGJpbmRtb2JpbGVtZW51KCk7XHJcbiAgfSk7XHJcblxyXG4gICQuZ2V0KHRtcHBhdGgrXCJpbmNsdWRlcy9mb290ZXItdGguaHRtbFwiLGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgdmFyIHJlcyA9IFwiXCI7XHJcbiAgICByZXMgPSBkYXRhLnJlcGxhY2UobmV3IFJlZ0V4cChcIi4uL2Rpc3QvXCIsXCJnXCIpLFwiZGlzdC9cIik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiZGlzdC9cIixcImdcIiksdG1wcGF0aCtcImRpc3QvXCIpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi90aC9cIixcImdcIiksY3Vycmhvc3QpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi9lbi9cIixcImdcIiksY3Vycmhvc3Rlbik7XHJcbiAgICAkKFwiI2Zvb3RlclwiKS5yZXBsYWNlV2l0aChyZXMpO1xyXG4gICAgaW5pdGdsb2JhbChoZWFkbnVtKTtcclxuICB9KTtcclxuXHJcbiAgJC5nZXQodG1wcGF0aCtcImluY2x1ZGVzL3NpZGUtbWVudS10aC5odG1sXCIsZnVuY3Rpb24oZGF0YSl7XHJcbiAgICB2YXIgcmVzID0gXCJcIjtcclxuICAgIHJlcyA9IGRhdGEucmVwbGFjZShuZXcgUmVnRXhwKFwiLi4vZGlzdC9cIixcImdcIiksXCJkaXN0L1wiKTtcclxuICAgIHJlcyA9IHJlcy5yZXBsYWNlKG5ldyBSZWdFeHAoXCJkaXN0L1wiLFwiZ1wiKSx0bXBwYXRoK1wiZGlzdC9cIik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiL3RoL1wiLFwiZ1wiKSxjdXJyaG9zdCk7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiL2VuL1wiLFwiZ1wiKSxjdXJyaG9zdGVuKTtcclxuICAgICQoXCIjc2lkZW1lbnVcIikucmVwbGFjZVdpdGgocmVzKTtcclxuICAgIGluaXRnbG9iYWwoaGVhZG51bSk7XHJcbiAgICBpbml0ZmFxYmxvY2soKTtcclxuICB9KTtcclxuXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGxvYWRoZWFkZXJmb290ZXJlbih0bXBwYXRoLGhlYWRudW0pe1xyXG4gICQuZ2V0KHRtcHBhdGgrXCJpbmNsdWRlcy9oZWFkZXItZW4uaHRtbFwiLGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgdmFyIHJlcyA9IFwiXCI7XHJcbiAgICByZXMgPSBkYXRhLnJlcGxhY2UobmV3IFJlZ0V4cChcIi4uL2Rpc3QvXCIsXCJnXCIpLFwiZGlzdC9cIik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiZGlzdC9cIixcImdcIiksdG1wcGF0aCtcImRpc3QvXCIpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi9lbi9cIixcImdcIiksY3Vycmhvc3Rlbik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiL3RoL1wiLFwiZ1wiKSxjdXJyaG9zdCk7XHJcbiAgICAkKFwiI2hlYWRlclwiKS5yZXBsYWNlV2l0aChyZXMpO1xyXG4gICAgaW5pdGdsb2JhbChoZWFkbnVtKTtcclxuICAgIGJpbmRtb2JpbGVtZW51KCk7XHJcbiAgfSk7XHJcblxyXG4gICQuZ2V0KHRtcHBhdGgrXCJpbmNsdWRlcy9mb290ZXItZW4uaHRtbFwiLGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgdmFyIHJlcyA9IFwiXCI7XHJcbiAgICByZXMgPSBkYXRhLnJlcGxhY2UobmV3IFJlZ0V4cChcIi4uL2Rpc3QvXCIsXCJnXCIpLFwiZGlzdC9cIik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiZGlzdC9cIixcImdcIiksdG1wcGF0aCtcImRpc3QvXCIpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi9lbi9cIixcImdcIiksY3Vycmhvc3Rlbik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiL3RoL1wiLFwiZ1wiKSxjdXJyaG9zdCk7XHJcbiAgICAkKFwiI2Zvb3RlclwiKS5yZXBsYWNlV2l0aChyZXMpO1xyXG4gICAgaW5pdGdsb2JhbChoZWFkbnVtKTtcclxuICB9KTtcclxuICAgICQuZ2V0KHRtcHBhdGgrXCJpbmNsdWRlcy9zaWRlLW1lbnUtZW4uaHRtbFwiLGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgdmFyIHJlcyA9IFwiXCI7XHJcbiAgICByZXMgPSBkYXRhLnJlcGxhY2UobmV3IFJlZ0V4cChcIi4uL2Rpc3QvXCIsXCJnXCIpLFwiZGlzdC9cIik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiZGlzdC9cIixcImdcIiksdG1wcGF0aCtcImRpc3QvXCIpO1xyXG4gICAgcmVzID0gcmVzLnJlcGxhY2UobmV3IFJlZ0V4cChcIi9lbi9cIixcImdcIiksY3Vycmhvc3Rlbik7XHJcbiAgICByZXMgPSByZXMucmVwbGFjZShuZXcgUmVnRXhwKFwiL3RoL1wiLFwiZ1wiKSxjdXJyaG9zdCk7XHJcbiAgICAkKFwiI3NpZGVtZW51XCIpLnJlcGxhY2VXaXRoKHJlcyk7XHJcbiAgICBpbml0Z2xvYmFsKGhlYWRudW0pO1xyXG4gICAgaW5pdGZhcWJsb2NrKCk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gZnVuY3Rpb24gZ2xvYmFsIGluaXQgLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuZnVuY3Rpb24gaW5pdGdsb2JhbChoZWFkbnVtKXtcclxuICAgIC8vYmluZG1vYmlsZW1lbnUoKTtcclxuICAgIG9wZW5oZWFkZXJsdjEoaGVhZG51bSk7XHJcbiAgICBvcGVuaGVhZGVybHYyKCk7XHJcbiAgICAvL2luaXRmYXFibG9jaygpO1xyXG4gICAgc2xpZGVvdXRtZW51KCk7XHJcbiAgICBpZigkKHdpbmRvdykud2lkdGgoKTwxMzI0KXtcclxuICAgICAgICAkKFwiLmhlYWRlcmx2MS5sb2dvXCIpLndpZHRoKDE0NSk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgICAkKFwiLmhlYWRlcmx2MS5sb2dvXCIpLndpZHRoKDIxMCk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKXtcclxuICAgICAgICBpZigkKHdpbmRvdykud2lkdGgoKTwxMzI0KXtcclxuICAgICAgICAgICAgJChcIi5oZWFkZXJsdjEubG9nb1wiKS53aWR0aCgxNDUpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAkKFwiLmhlYWRlcmx2MS5sb2dvXCIpLndpZHRoKDIxMCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn1cclxuXHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1mdW5jdGlvbiBmb3IgaGVhZGVyIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbnZhciBjdXJyZW50aGVhZGVybHYxID0gJyc7XHJcbnZhciB0ZW1wY3VycmVudGhlYWQgPSAnJztcclxuZnVuY3Rpb24gb3BlbmhlYWRlcmx2MShvYmpuYW1lKXtcclxuICAgIC8vIGlmKG9iam5hbWUhPWN1cnJlbnRoZWFkZXJsdjEpe1xyXG4gICAgICAgIGlmKG9iam5hbWUgPT0gJ3NldDAnKXtcclxuICAgICAgICAgICAgb2JqbmFtZSA9ICdzZXQxJztcclxuICAgICAgICB9XHJcbiAgICAgICAgY3VycmVudGhlYWRlcmx2MSA9IG9iam5hbWU7XHJcbiAgICAgICAgJChcIi5oZWFkZXJsdjEuc2hvd21lbnVcIikucmVtb3ZlQ2xhc3MoXCJzaG93bWVudVwiKS5hZGRDbGFzcyhcImhpZGVtZW51XCIpO1xyXG5cclxuICAgICAgICAkKFwiLmhmaXJzdGxpbmtcIikucmVtb3ZlQ2xhc3MoXCJpc3NlbGVjdGVkXCIpO1xyXG4gICAgICAgICQoXCIuaGZpcnN0bGluay5cIitvYmpuYW1lKS5hZGRDbGFzcyhcImlzc2VsZWN0ZWRcIik7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gY2hlY2sgaWYgY2hhbmdlIG1lbnUgLHdpbGwgZmFkZSBvdXQgXHJcbiAgICAgICAgaWYodGVtcGN1cnJlbnRoZWFkIT1jdXJyZW50aGVhZGVybHYxKXtcclxuICAgICAgICAgICAgJChcIi5oZWFkZXJsdjEuaGlkZW1lbnVcIikuZmFkZU91dCg1MDAsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgfSkuY3NzKFwiZGlzcGxheVwiLFwibm9uZVwiKTtcclxuXHJcbiAgICAgICAgICAgIHRlbXBjdXJyZW50aGVhZCA9IGN1cnJlbnRoZWFkZXJsdjE7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAkKFwiLmhlYWRlcmx2MS5cIitvYmpuYW1lKS5mYWRlSW4oNTAwLGZ1bmN0aW9uKCl7XHJcblxyXG4gICAgICAgIH0pLmNzcyhcImRpc3BsYXlcIixcImlubGluZS1ibG9ja1wiKTtcclxuXHJcbiAgICAvLyB9XHJcbn1cclxuLyoqL1xyXG52YXIgY3VycmVudGhlYWRlcmx2MiA9ICcnO1xyXG52YXIgaW50ZXJ2YWxyb2xsb3V0O1xyXG52YXIgY291bnRob3ZlciA9IDA7XHJcbmZ1bmN0aW9uIG9wZW5oZWFkZXJsdjIob2JqbmFtZSl7XHJcblxyXG4gICAgaWYoY3VycmVudGhlYWRlcmx2MiE9b2JqbmFtZSl7XHJcblxyXG4gICAgICAgIGN1cnJlbnRoZWFkZXJsdjIgPSBvYmpuYW1lO1xyXG4gICAgICAgICQoXCIuaGVhZGVybHYyXCIpLmhpZGUoKTtcclxuICAgICAgICAkKFwiLmhzZWNvbmRsaW5rXCIpLnJlbW92ZUNsYXNzKFwiaXNzZWxlY3RlZFwiKTtcclxuICAgICAgICAkKFwiLmhzZWNvbmRsaW5rLlwiK29iam5hbWUpLmFkZENsYXNzKFwiaXNzZWxlY3RlZFwiKTtcclxuXHJcbiAgICAgICAgY291bnR0aW1lcm9sbG91dChvYmpuYW1lKTtcclxuICAgICAgICBcclxuICAgICAgICAvLyAkKFwiLmhlYWRlcmx2Mi5cIitvYmpuYW1lKS5zbGlkZURvd24oNTAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICAgICAgaWYoY291bnRob3ZlciA+IDEpe1xyXG4gICAgICAgICAgICAkKFwiLmhlYWRlcmx2Mi5cIitvYmpuYW1lKS5zaG93KCk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICQoXCIuaGVhZGVybHYyLlwiK29iam5hbWUpLmZhZGVJbigyMDApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb3VudGhvdmVyICs9IDE7XHJcblxyXG4gICAgfVxyXG4gICAgLy8gZWxzZXtcclxuICAgIC8vICAgICBjYWxsYmFja2Zyb21pbnRlcnZhbCgpO1xyXG4gICAgLy8gfVxyXG4gICAgXHJcbn1cclxuZnVuY3Rpb24gY2xvc2VoZWFkZXJsdjIoKXtcclxuICAgIGN1cnJlbnRoZWFkZXJsdjIgPSAnJztcclxuICAgICQoXCIuaHNlY29uZGxpbmtcIikucmVtb3ZlQ2xhc3MoXCJpc3NlbGVjdGVkXCIpO1xyXG4gICAgLy8gJChcIi5oZWFkZXJsdjJcIikuc2xpZGVVcCg1MDAsZnVuY3Rpb24oKXtcclxuXHJcbiAgICAvLyB9KTtcclxuICAgICQoXCIuaGVhZGVybHYyXCIpLmZhZGVPdXQoMTAwKTtcclxuICAgIGNvdW50aG92ZXIgPSAxO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjYWxsYmFja2Zyb21pbnRlcnZhbCgpe1xyXG4gICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbHJvbGxvdXQpO1xyXG4gICAgY2xvc2VoZWFkZXJsdjIoKTtcclxufVxyXG5cclxuZnVuY3Rpb24gY291bnR0aW1lcm9sbG91dChvYmpuYW1lKXtcclxuXHJcbiAgIC8qIHZhciBpbm1lbnUgPSB0cnVlO1xyXG5cclxuICAgICQoJy5oZWFkZXJsdjIuJytvYmpuYW1lKS5tb3VzZWxlYXZlKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgaW5tZW51ID0gZmFsc2U7XHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgJCgnLmhlYWRlcmx2Mi4nK29iam5hbWUpLm1vdXNlbW92ZShmdW5jdGlvbigpe1xyXG4gICAgICAgIGlmKCFpbm1lbnUpe1xyXG4gICAgICAgICAgICBjbGVhckludGVydmFsKGludGVydmFscm9sbG91dCk7XHJcbiAgICAgICAgICAgIGludGVydmFscm9sbG91dCA9IHNldEludGVydmFsKGNhbGxiYWNrZnJvbWludGVydmFsLDE0MDApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgIH0pOyovXHJcblxyXG4gICAgXHJcbiAgICAkKCcuaGVhZGVybHYyLicrb2JqbmFtZSkubW91c2VsZWF2ZShmdW5jdGlvbigpe1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwoaW50ZXJ2YWxyb2xsb3V0KTtcclxuICAgICAgICBpbnRlcnZhbHJvbGxvdXQgPSBzZXRJbnRlcnZhbChjYWxsYmFja2Zyb21pbnRlcnZhbCw0MDApO1xyXG4gICAgfSk7XHJcblxyXG4gICAgXHJcbn1cclxuXHJcbi8vIGZ1bmN0aW9uIGNsb3NlaGVhZGVybHYyKG9iam5hbWUpe1xyXG4vLyAgICAgY3VycmVudGhlYWRlcmx2MiA9ICcnO1xyXG4vLyAgICAgJChcIi5oc2Vjb25kbGlua1wiKS5yZW1vdmVDbGFzcyhcImlzc2VsZWN0ZWRcIik7XHJcbi8vICAgICAkKFwiLmhlYWRlcmx2Mi5cIitvYmpuYW1lKS5zbGlkZVVwKDUwMCxmdW5jdGlvbigpe1xyXG4vLyAgICAgfSk7XHJcbi8vIH1cclxuXHJcbi8vIHZhciBjdXJyb3Blbm9iajtcclxuXHJcbi8vIGZ1bmN0aW9uIGNhbGxiYWNrZnJvbWludGVydmFsKCl7XHJcbiAgIFxyXG4vLyAgICAgJCgnLmhlYWRlcmx2Mi4nK2N1cnJvcGVub2JqKS5tb3VzZWxlYXZlKGZ1bmN0aW9uKCl7XHJcbi8vICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbHJvbGxvdXQpO1xyXG4vLyAgICAgICAgIGNsb3NlaGVhZGVybHYyKGN1cnJvcGVub2JqKTtcclxuLy8gICAgIH0pO1xyXG4gICBcclxuLy8gfVxyXG4vLyBmdW5jdGlvbiBjb3VudHRpbWVyb2xsb3V0KG9iam5hbWUpe1xyXG4gICAgXHJcbi8vICAgICAkKCcuaGVhZGVybHYyLicrb2JqbmFtZSkubW91c2Vtb3ZlKGZ1bmN0aW9uKCl7XHJcbi8vICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbHJvbGxvdXQpO1xyXG4vLyAgICAgICAgIGN1cnJvcGVub2JqID0gb2JqbmFtZTtcclxuLy8gICAgICAgICBpbnRlcnZhbHJvbGxvdXQgPSBzZXRJbnRlcnZhbChjYWxsYmFja2Zyb21pbnRlcnZhbCwxNDAwKTtcclxuLy8gICAgIH0pO1xyXG4vLyB9XHJcbi8qKi9cclxuXHJcblxyXG5mdW5jdGlvbiBvcGVuZGl2bGFuZygpe1xyXG4gICAgJChcIi5kaXZjbGlja2xhbmdcIikudG9nZ2xlQ2xhc3MoXCJzZWxlY3RlZFwiKTtcclxuICAgIGlmKCQoXCIuZGl2Y2xpY2tsYW5nXCIpLmhhc0NsYXNzKFwic2VsZWN0ZWRcIikpe1xyXG4gICAgICAgICQoXCIuY2hvaWNlb3B0aW9uc1wiKS5zbGlkZURvd24oMzAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgICAkKFwiLmNob2ljZW9wdGlvbnNcIikuc2xpZGVVcCgzMDAsZnVuY3Rpb24oKXt9KTtcclxuICAgIH1cclxuICAgIFxyXG59XHJcblxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gZnVuY3Rpb24gZm9yIGZvb3RlciAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuZnVuY3Rpb24gb3BlbmZvb3RlcihpZHgpe1xyXG4gICAgXHJcbiAgaWYoJCh3aW5kb3cpLndpZHRoKCk8OTkyKXtcclxuXHJcbiAgICBpZigkKCcuZm9vdGVybHYxOmVxKCcraWR4KycpID4gbGkgPiBhJykuaGFzQ2xhc3MoXCJpc3NlbGVjdGVkXCIpKXtcclxuICAgICAgJCgnLmZvb3Rlcmx2MTplcSgnK2lkeCsnKSA+IGxpID4gYScpLnJlbW92ZUNsYXNzKFwiaXNzZWxlY3RlZFwiKTtcclxuICAgIH1lbHNle1xyXG4gICAgICAkKCcuZm9vdGVybHYxOmVxKCcraWR4KycpID4gbGkgPiBhJykuYWRkQ2xhc3MoXCJpc3NlbGVjdGVkXCIpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAkKCcuZm9vdGVybHYyOmVxKCcraWR4KycpJykuc2xpZGVUb2dnbGUoXCJmYXN0XCIsZnVuY3Rpb24oKXtcclxuICAgICAgICAvKi0tLS0tIEZvciBJRTggLS0tLS0qL1xyXG4gICAgICAgIGlmKCQuYnJvd3Nlci5tc2llICYmIHBhcnNlRmxvYXQoJC5icm93c2VyLnZlcnNpb24pPD04KXtcclxuICAgICAgICAgICAgJCgnLmZvb3Rlcmx2MTplcSgnK2lkeCsnKSA+IGxpID4gYScpLnRvZ2dsZUNsYXNzKFwiZm9vdGVybWJjbG9zZVwiKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgfVxyXG59XHJcblxyXG4kKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7XHJcbiAgJCgnLmZvb3Rlcmx2MSA+IGxpID4gYScpLnJlbW92ZUNsYXNzKFwiaXNzZWxlY3RlZFwiKTtcclxuICBpZigkKHdpbmRvdykud2lkdGgoKTw5OTIpe1xyXG4gICAgLyotLS0tLSBGb3IgSUU4IC0tLS0tKi9cclxuICAgIGlmKCQuYnJvd3Nlci5tc2llICYmIHBhcnNlRmxvYXQoJC5icm93c2VyLnZlcnNpb24pPD04KXtcclxuICAgICAgICAkKCcuZm9vdGVybHYxID4gbGkgPiBhIGknKS5oaWRlKCk7XHJcbiAgICAgICAgJCgnLmZvb3Rlcmx2MSA+IGxpID4gYScpLmFkZENsYXNzKCdmb290ZXJtYm9wZW4nKTtcclxuICAgIH1lbHNle1xyXG4gICAgICAgICQoJy5mb290ZXJsdjInKS5jc3MoXCJkaXNwbGF5XCIsXCJub25lXCIpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgfWVsc2V7ICAgIFxyXG4gICAgJCgnLmZvb3Rlcmx2MicpLnNob3coKTtcclxuICB9XHJcbn0pO1xyXG5cclxuLypmb3IgbW9iaWxlICovXHJcbmZ1bmN0aW9uIGJpbmRtb2JpbGVtZW51KCl7XHJcblxyXG4gICAgLyotLS0tLSBGb3IgSUU4IC0tLS0tKi9cclxuICAgIGlmKCQuYnJvd3Nlci5tc2llICYmIHBhcnNlRmxvYXQoJC5icm93c2VyLnZlcnNpb24pPD04KXtcclxuICAgICAgICAkKCcjbWVudS1tb2JpbGUtYnV0dG9uJykuYWRkQ2xhc3MoJ2hlYWRlcm1ib3BlbicpO1xyXG4gICAgfVxyXG5cclxuICAgICQoJyNtZW51LW1vYmlsZS1idXR0b24nKS5jbGljayhmdW5jdGlvbigpe1xyXG4gICAgICAgIC8qLS0tLS0gRm9yIElFOCAtLS0tLSovXHJcbiAgICAgICAgaWYoJC5icm93c2VyLm1zaWUgJiYgcGFyc2VGbG9hdCgkLmJyb3dzZXIudmVyc2lvbik8PTgpe1xyXG4gICAgICAgICAgICAkKCcjbWVudS1tb2JpbGUtYnV0dG9uJykudG9nZ2xlQ2xhc3MoJ2hlYWRlcm1iY2xvc2UnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcclxuICAgICAgICBpZigkKHRoaXMpLmhhc0NsYXNzKCdvcGVuJykpe1xyXG4gICAgICAgICAgICAkKFwiLnNlY29uZG1lbnVtYlwiKS5mYWRlSW4oMzUwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICAgICAgICAgICQoXCIudGhpcmRtZW51bWJcIikuc2xpZGVEb3duKDMwMCxmdW5jdGlvbigpe30pO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAkKCcuZGl2bGFuZ3VhZ2VtYicpLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcbiAgICAgICAgICAgICQoXCIuZGl2bGFuZ3VhZ2VjaG9pY2VcIikuc2xpZGVVcCgyMDAsZnVuY3Rpb24oKXt9KTtcclxuICAgICAgICAgICAgJChcIi5kaXZsdnN1Ym1haW5cIikuaGlkZSgpO1xyXG4gICAgICAgICAgICAkKCcubHYxbWJsaW5rJykucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcbiAgICAgICAgICAgICQoJy5sdjInKS5zbGlkZVVwKDIwMCxmdW5jdGlvbigpe30pO1xyXG4gICAgICAgICAgICAkKFwiLnRoaXJkbWVudW1iXCIpLnNsaWRlVXAoMjAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICAgICAgICAgICQoXCIuc2Vjb25kbWVudW1iXCIpLmZhZGVPdXQoMTAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICAgICAgICAgICQoXCIudGhpcmRtZW51bWJcIikuY3NzKHtsZWZ0OlwiMCVcIn0pO1xyXG4gICAgICAgICAgICAkKFwiLmZvdXJ0aG1lbnVtYlwiKS5jc3Moe2xlZnQ6XCIxMDAlXCJ9KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuICAgICQoJy5kaXZsYW5ndWFnZW1iJykuY2xpY2soZnVuY3Rpb24oKXtcclxuICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XHJcbiAgICAgICAgaWYoJCh0aGlzKS5oYXNDbGFzcygnb3BlbicpKXtcclxuICAgICAgICAgICAgJChcIi5kaXZsYW5ndWFnZWNob2ljZVwiKS5zbGlkZURvd24oMjAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICQoXCIuZGl2bGFuZ3VhZ2VjaG9pY2VcIikuc2xpZGVVcCgyMDAsZnVuY3Rpb24oKXt9KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuICAgIFxyXG59XHJcbmZ1bmN0aW9uIG9wZW5tb2JpbGVzdWJtZW51KG9iam5hbWUpe1xyXG4gICAgJChcIi5kaXZsdnN1Ym1haW5cIikuaGlkZSgpO1xyXG4gICAgJChcIi5cIitvYmpuYW1lKS5zaG93KCk7XHJcbiAgICAkKFwiLnRoaXJkbWVudW1iXCIpLmFuaW1hdGUoe2xlZnQ6XCItMTAwJVwifSw1MDAsZnVuY3Rpb24oKXt9KTtcclxuICAgICQoXCIuZm91cnRobWVudW1iXCIpLmFuaW1hdGUoe2xlZnQ6XCIwJVwifSw1MDAsZnVuY3Rpb24oKXt9KTtcclxufVxyXG5mdW5jdGlvbiBiYWNrbW9iaWxlc3VibWVudSgpe1xyXG4gICAgJChcIi50aGlyZG1lbnVtYlwiKS5hbmltYXRlKHtsZWZ0OlwiMCVcIn0sNTAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICAkKFwiLmZvdXJ0aG1lbnVtYlwiKS5hbmltYXRlKHtsZWZ0OlwiMTAwJVwifSw1MDAsZnVuY3Rpb24oKXt9KTtcclxuICAgICQoJy5sdjFtYmxpbmsnKS5yZW1vdmVDbGFzcygnc2VsZWN0ZWQnKTtcclxuICAgICQoJy5sdjInKS5zbGlkZVVwKDIwMCxmdW5jdGlvbigpe30pO1xyXG59XHJcblxyXG52YXIgc3Vic2V0bHYyID0gJyc7XHJcbnZhciBudW1vcGVuID0gMDtcclxuZnVuY3Rpb24gb3Blbm1vYmlsZWx2Mm1lbnUob2JqbmFtZSl7XHJcbiAgICAkKCcubHYxbWJsaW5rJykucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcbiAgICAkKCcubHYyJykuc2xpZGVVcCgyMDAsZnVuY3Rpb24oKXt9KTtcclxuXHJcbiAgICBpZihzdWJzZXRsdjIgIT0gb2JqbmFtZSl7XHJcbiAgICAgICAgJCgnLicrb2JqbmFtZSsnbGluaycpLmFkZENsYXNzKCdzZWxlY3RlZCcpO1xyXG4gICAgICAgICQoJy4nK29iam5hbWUpLnNsaWRlRG93bigyMDAsZnVuY3Rpb24oKXt9KTtcclxuICAgICAgICBzdWJzZXRsdjIgPSBvYmpuYW1lO1xyXG4gICAgICAgIG51bW9wZW4gPSAwOyAvL3NldCBzdGFydCAwIGFnYWluXHJcbiAgICB9ZWxzZXtcclxuICAgICAgICBpZihudW1vcGVuIDwgMSl7XHJcbiAgICAgICAgICAgIG51bW9wZW4gPSBudW1vcGVuKzE7IFxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAkKCcuJytvYmpuYW1lKydsaW5rJykuYWRkQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcbiAgICAgICAgICAgICQoJy4nK29iam5hbWUpLnNsaWRlRG93bigyMDAsZnVuY3Rpb24oKXt9KTtcclxuICAgICAgICAgICAgbnVtb3BlbiA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgfSAgICBcclxufVxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gZnVuY3Rpb24gZm9yIHNpZGUgbWVudSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmZ1bmN0aW9uIHNsaWRlb3V0bWVudSgpe1xyXG4gICAgJCgnLnNpZGVtZW51JykuaG92ZXIoZnVuY3Rpb24oKXtcclxuICAgICAgICAkKCcudGFicy1tZW51Jykuc3RvcCh0cnVlLGZhbHNlKS5hbmltYXRlKHtcclxuICAgICAgICAgICAgcmlnaHQ6JzQwcHgnXHJcbiAgICAgICAgfSw4MDApO1xyXG4gICAgfSxmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJy50YWJzLW1lbnUnKS5zdG9wKHRydWUsZmFsc2UpLmFuaW1hdGUoe1xyXG4gICAgICAgICAgICByaWdodDonLTE4MHB4J1xyXG4gICAgICAgIH0sODAwKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIGZ1bmN0aW9uIGZvciBTaGFyZSBidXR0b24gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmZ1bmN0aW9uIHNoYXJlcGFnZShvYmpuYW1lKXtcclxuICBcclxuICAkKCcjJytvYmpuYW1lKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcblxyXG4gIHZhciBzb2NpYWx3aWR0aCA9ICQoJyMnK29iam5hbWUpLnBhcmVudCgpLnBhcmVudCgpLmZpbmQoXCIuZm9sbG93c29jaWFsXCIpLndpZHRoKCk7XHJcblxyXG4gIGlmKCQoJyMnK29iam5hbWUpLmhhc0NsYXNzKCdhY3RpdmUnKSl7XHJcbiAgICAkKCcjJytvYmpuYW1lKS5wYXJlbnQoKS5wYXJlbnQoKS5maW5kKCcuc2hhcmUtcGFsZXR0ZScpLmFuaW1hdGUoe1xyXG4gICAgICAgIG1hcmdpbkxlZnQ6ICctPScrKHNvY2lhbHdpZHRoKzUpKydweCcsXHJcbiAgICAgICAgd2lkdGg6c29jaWFsd2lkdGgrJ3B4J1xyXG4gICAgfSw1MDApO1xyXG4gICAgXHJcbiAgfWVsc2V7XHJcbiAgICAkKCcjJytvYmpuYW1lKS5wYXJlbnQoKS5wYXJlbnQoKS5maW5kKCcuc2hhcmUtcGFsZXR0ZScpLmFuaW1hdGUoe1xyXG4gICAgICAgIG1hcmdpbkxlZnQ6ICcwcHgnLFxyXG4gICAgICAgIHdpZHRoOiAnMHB4J1xyXG4gICAgfSw1MDApO1xyXG4gIH1cclxufVxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLSBNYXRjaCBoZWlnaHQgZm9yIGFsbCBjb2x1bW5zIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG5mdW5jdGlvbiBtYXRjaGhlaWdodCgpe1xyXG4gICAgdmFyIG1heGhlaWdodCA9IDA7XHJcbiAgICAkKFwiLmNvbnRlbnRsaXN0ID4gZGl2XCIpLmNzcyhcImhlaWdodFwiLFwiYXV0b1wiKTtcclxuICAgICQoXCIuY29udGVudGxpc3QgPiBkaXZcIikuZWFjaChmdW5jdGlvbihpbmRleCl7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZygkKFwiLmNvbnRlbnRsaXN0ID4gZGl2OmVxKFwiK2luZGV4K1wiKVwiKS5oZWlnaHQoKSk7XHJcbiAgICAgICAgaWYoJChcIi5jb250ZW50bGlzdCA+IGRpdjplcShcIitpbmRleCtcIilcIikuaGVpZ2h0KCk+bWF4aGVpZ2h0KXtcclxuICAgICAgICAgICAgbWF4aGVpZ2h0ID0gJChcIi5jb250ZW50bGlzdCA+IGRpdjplcShcIitpbmRleCtcIilcIikuaGVpZ2h0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICAkKFwiLmNvbnRlbnRsaXN0ID4gZGl2XCIpLmhlaWdodChtYXhoZWlnaHQpO1xyXG4gICAgJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe1xyXG4gICAgICAgIG1heGhlaWdodCA9IDA7XHJcbiAgICAgICAgJChcIi5jb250ZW50bGlzdCA+IGRpdlwiKS5jc3MoXCJoZWlnaHRcIixcImF1dG9cIik7XHJcbiAgICAgICAgJChcIi5jb250ZW50bGlzdCA+IGRpdlwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZygkKFwiLmNvbnRlbnRsaXN0ID4gZGl2OmVxKFwiK2luZGV4K1wiKVwiKS5oZWlnaHQoKSk7XHJcbiAgICAgICAgICAgIGlmKCQoXCIuY29udGVudGxpc3QgPiBkaXY6ZXEoXCIraW5kZXgrXCIpXCIpLmhlaWdodCgpPm1heGhlaWdodCl7XHJcbiAgICAgICAgICAgICAgICBtYXhoZWlnaHQgPSAkKFwiLmNvbnRlbnRsaXN0ID4gZGl2OmVxKFwiK2luZGV4K1wiKVwiKS5oZWlnaHQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQoXCIuY29udGVudGxpc3QgPiBkaXZcIikuaGVpZ2h0KG1heGhlaWdodCk7XHJcbiAgICB9KTtcclxufVxyXG4vKiovXHJcbmZ1bmN0aW9uIG1hdGNoaGVpZ2h0Migpe1xyXG5cclxuICAgIC8vIHZhciBtYXhoZWlnaHRzbSA9IDA7XHJcbiAgICAvLyB2YXIgbWF4aGVpZ2h0bGcgPSAwO1xyXG5cclxuICAgIC8vICQoXCIuY29udGVudGxpc3QgPiBkaXYuaXRtbGdcIikuY3NzKFwiaGVpZ2h0XCIsXCJhdXRvXCIpO1xyXG4gICAgLy8gJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1zbVwiKS5jc3MoXCJoZWlnaHRcIixcImF1dG9cIik7XHJcblxyXG4gICAgXHJcbiAgICAvLyAkKFwiLmNvbnRlbnRsaXN0ID4gZGl2Lml0bWxnXCIpLmhlaWdodChtYXhoZWlnaHRsZyk7XHJcblxyXG4gICAgLy8gJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1zbVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtcclxuICAgIC8vICAgICBpZigkKFwiLmNvbnRlbnRsaXN0ID4gZGl2Lml0bXNtOmVxKFwiK2luZGV4K1wiKVwiKS5oZWlnaHQoKT5tYXhoZWlnaHRzbSl7XHJcbiAgICAvLyAgICAgICAgIG1heGhlaWdodHNtID0gJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1zbTplcShcIitpbmRleCtcIilcIikuaGVpZ2h0KCk7XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfSk7XHJcbiAgICAvLyAkKFwiLmNvbnRlbnRsaXN0ID4gZGl2Lml0bXNtXCIpLmhlaWdodChtYXhoZWlnaHRzbSk7XHJcblxyXG4gICAgLy8gJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe1xyXG4gICAgLy8gICAgICBtYXhoZWlnaHRzbSA9IDA7XHJcbiAgICAvLyAgICAgIG1heGhlaWdodGxnID0gMDtcclxuXHJcbiAgICAvLyAgICAgJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1sZ1wiKS5jc3MoXCJoZWlnaHRcIixcImF1dG9cIik7XHJcbiAgICAvLyAgICAgJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1zbVwiKS5jc3MoXCJoZWlnaHRcIixcImF1dG9cIik7XHJcblxyXG4gICAgICAgXHJcbiAgICAvLyAgICAgJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1sZ1wiKS5oZWlnaHQobWF4aGVpZ2h0bGcpO1xyXG5cclxuICAgIC8vICAgICAkKFwiLmNvbnRlbnRsaXN0ID4gZGl2Lml0bXNtXCIpLmVhY2goZnVuY3Rpb24oaW5kZXgpe1xyXG4gICAgLy8gICAgICAgICBpZigkKFwiLmNvbnRlbnRsaXN0ID4gZGl2Lml0bXNtOmVxKFwiK2luZGV4K1wiKVwiKS5oZWlnaHQoKT5tYXhoZWlnaHRzbSl7XHJcbiAgICAvLyAgICAgICAgICAgICBtYXhoZWlnaHRzbSA9ICQoXCIuY29udGVudGxpc3QgPiBkaXYuaXRtc206ZXEoXCIraW5kZXgrXCIpXCIpLmhlaWdodCgpO1xyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgfSk7XHJcbiAgICAvLyAgICAgJChcIi5jb250ZW50bGlzdCA+IGRpdi5pdG1zbVwiKS5oZWlnaHQobWF4aGVpZ2h0c20pO1xyXG4gICAgLy8gfSk7XHJcblxyXG5cclxufVxyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tIGZvciBTbGljayB0ZW1wbGF0ZSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuICQoZnVuY3Rpb24oKXtcclxuXHJcbiAgICBtb3Zlc2xpY2t0eXBlMigpOyAvKmN1c3RvbSBuYXZpZ2F0b3IgZm9yIHNsaWNrYmxvY2syKi8gXHJcblxyXG4gICAgaWYocGFyc2VGbG9hdCgkLmJyb3dzZXIudmVyc2lvbikgPiA4KXtcclxuICAgICAgICAkKCcuc2xpY2tibG9jazEnKS5zbGljayh7XHJcbiAgICAgICAgICAgIGRvdHM6dHJ1ZSxcclxuICAgICAgICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgICAgIGF1dG9wbGF5U3BlZWQ6IDMwMDAsXHJcbiAgICAgICAgICAgIGluZmluaXRlOnRydWUsXHJcbiAgICAgICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgc3dpcGVUb1NsaWRlOnRydWVcclxuICAgICAgICB9KTtcclxuICAgIH1lbHNle1xyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrMScpLnNsaWNrKHtcclxuICAgICAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgICAgICBhdXRvcGxheTogZmFsc2UsXHJcbiAgICAgICAgICAgIGluZmluaXRlOnRydWUsXHJcbiAgICAgICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgc3dpcGVUb1NsaWRlOnRydWVcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazInKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzo0LFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOjQsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbe1xyXG4gICAgICAgICAgICBicmVha3BvaW50OjEyMDAsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzozLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6M1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6OTkyLFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MixcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjJcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9LHtcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo3NjgsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoyLjUsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDoyXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSxdXHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazMnKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIGluaXRpYWxTbGlkZTogMCxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6Mi41LFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOjIsXHJcbiAgICAgICAgdmFyaWFibGVXaWR0aDogdHJ1ZSxcclxuICAgICAgICBhZGFwdGl2ZUhlaWdodDogdHJ1ZSxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFt7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY4LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MSxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjEsXHJcbiAgICAgICAgICAgICAgICBpbml0aWFsU2xpZGU6IDAsXHJcbiAgICAgICAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGFkYXB0aXZlSGVpZ2h0OiBmYWxzZSxcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XVxyXG4gICAgICAgIFxyXG4gICAgfSk7IFxyXG5cclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazQnKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzozLFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOjMsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbe1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojk5MixcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjIsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDoyXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY4LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MS4zLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6MVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfV1cclxuICAgIH0pO1xyXG4gICAgJCgnLnNsaWNrYmxvY2s1Jykuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6dHJ1ZSxcclxuICAgICAgICBpbmZpbml0ZTpmYWxzZSxcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6NCxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDo0LFxyXG4gICAgICAgIHJlc3BvbnNpdmU6XHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo3NjgsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoyLjUsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDoyLFxyXG4gICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgICBicmVha3BvaW50OjQ4MCxcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjIuNCxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjIsXHJcbiAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfV1cclxuICAgIH0pO1xyXG4gICAgJCgnLnNsaWNrYmxvY2s2Jykuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6dHJ1ZSxcclxuICAgICAgICBpbmZpbml0ZTp0cnVlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzozLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6XHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo3NjgsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoyLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6MixcclxuICAgICAgICAgICAgICAgIHN3aXBlOnRydWUsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XVxyXG4gICAgfSk7XHJcbiAgICAkKCcuc2xpY2tibG9jazcnKS5zbGljayh7XHJcbiAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMyxcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTogXHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDogNzY4LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEuNSxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgICAgICAgICAgZG90czogZmFsc2VcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBicmVha3BvaW50OiA0ODAsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMS4yLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICBkb3RzOiBmYWxzZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgICAgICBcclxuICAgIH0pO1xyXG4gICAgJCgnLnNsaWNrYmxvY2s4Jykuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6ZmFsc2UsXHJcbiAgICAgICAgaW5maW5pdGU6ZmFsc2UsXHJcbiAgICAgICAgYXJyb3dzOmZhbHNlLFxyXG4gICAgICAgIHN3aXBlOmZhbHNlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzozLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6XHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo3NjgsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjEuNSxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjEsXHJcbiAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxdXHJcbiAgICB9KTtcclxuICAgICAkKCcuc2xpY2tibG9jazknKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzoxLFxyXG4gICAgfSk7ICBcclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazEwLXByZXZpZXcnKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgZmFkZTogdHJ1ZSxcclxuICAgICAgICBhc05hdkZvcjogJy5zbGlja2Jsb2NrMTAtbmF2J1xyXG4gICAgfSk7XHJcblxyXG4gICAgJCgnLnNsaWNrYmxvY2sxMC1uYXYnKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiA1LFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgYXNOYXZGb3I6ICcuc2xpY2tibG9jazEwLXByZXZpZXcnLFxyXG4gICAgICAgIGZvY3VzT25TZWxlY3Q6IHRydWUsXHJcbiAgICAgICAgY2VudGVyTW9kZTogdHJ1ZSxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFt7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY4LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6M1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NDgwLFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfV1cclxuICAgIH0pO1xyXG4gICAgJCgnLnNsaWNrYmxvY2sxMScpLnNsaWNrKHtcclxuICAgICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiA3LFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiA3LFxyXG4gICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTogXHJcbiAgICAgICAgICAgIFt7XHJcbiAgICAgICAgICAgICAgICBicmVha3BvaW50OjEyMDEsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiA1LFxyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiA1XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6OTkzLFxyXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogNCxcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogNFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XVxyXG4gICAgfSk7XHJcbiAgICAgJCgnLnNsaWNrYmxvY2sxMicpLnNsaWNrKHtcclxuICAgICAgICBkb3RzOnRydWUsXHJcbiAgICAgICAgaW5maW5pdGU6ZmFsc2UsXHJcbiAgICAgICAgYXJyb3dzOmZhbHNlLFxyXG4gICAgICAgIHN3aXBlOnRydWUsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OjMsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6MyxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFt7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6OTkyLFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MixcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjJcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9LHtcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo3NjgsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoxLjMsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDoxXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XVxyXG4gICAgfSk7XHJcbiAgICAkKCcuc2xpY2tibG9jazEzJykuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6dHJ1ZSxcclxuICAgICAgICBpbmZpbml0ZTpmYWxzZSxcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6MyxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDozLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6XHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo5OTIsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoyLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6MlxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojc2OCxcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjEuMyxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjFcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1dXHJcbiAgICB9KTtcclxuICAgICQoJy5zbGlja2Jsb2NrMTQnKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzozLFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOjMsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbe1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojk5MixcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjIsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDoyXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY4LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MS4zLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6MVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfV1cclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5zbGlja2Jsb2NrMTUnKS5zbGljayh7XHJcbiAgICAgICAgZG90czpmYWxzZSxcclxuICAgICAgICBpbmZpbml0ZTpmYWxzZSxcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgc3dpcGU6ZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OjQsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbe1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojk5MixcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzozLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6M1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY4LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjEuMyxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjFcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1dXHJcbiAgICB9KTtcclxuICAgICQoJy5zbGlja2Jsb2NrMTYnKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzoxLFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOjFcclxuICAgIH0pO1xyXG4gICAgICQoJy5zbGlja2Jsb2NrMTcnKS5zbGljayh7XHJcbiAgICAgICAgZG90czpmYWxzZSxcclxuICAgICAgICBpbmZpbml0ZTpmYWxzZSxcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6MyxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDozLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6XHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo5OTIsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoyLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6MlxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojc2OCxcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjEuMyxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjFcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1dXHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazE4Jykuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6dHJ1ZSxcclxuICAgICAgICBpbmZpbml0ZTpmYWxzZSxcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICBpbml0aWFsU2xpZGU6IDAsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OjIuNSxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDoyLFxyXG4gICAgICAgIHZhcmlhYmxlV2lkdGg6IHRydWUsXHJcbiAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IHRydWUsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbe1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojc2OCxcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OjEsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDoxLFxyXG4gICAgICAgICAgICAgICAgaW5pdGlhbFNsaWRlOiAwLFxyXG4gICAgICAgICAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBhZGFwdGl2ZUhlaWdodDogZmFsc2UsXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfV1cclxuICAgICAgICBcclxuICAgIH0pOyBcclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazE5Jykuc2xpY2soe1xyXG4gICAgICAgIGRvdHM6ZmFsc2UsXHJcbiAgICAgICAgaW5maW5pdGU6dHJ1ZSxcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TaG93OjMsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6MSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHN3aXBlVG9TbGlkZTp0cnVlLFxyXG4gICAgICAgIGNlbnRlck1vZGU6IHRydWUsXHJcbiAgICAgICAgY2VudGVyUGFkZGluZzogJzBweCcsXHJcbiAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IHRydWUsXHJcbiAgICAgICAgdXNlVHJhbnNmb3JtOiB0cnVlLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6XHJcbiAgICAgICAgW3tcclxuICAgICAgICAgICAgYnJlYWtwb2ludDo3NjgsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzoxLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6MSxcclxuICAgICAgICAgICAgICAgIGNlbnRlck1vZGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdXNlVHJhbnNmb3JtOiBmYWxzZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfV1cclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5zbGlja2Jsb2NrMjAnKS5zbGljayh7XHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czpmYWxzZSxcclxuICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzo0LFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOjRcclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICAkKCcuc2xpY2tibG9jazknKS5vbignYmVmb3JlQ2hhbmdlJyxmdW5jdGlvbigpe1xyXG4gICAgICAgIG9wZW5waWMoKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5zbGlja2Jsb2NrOScpLm9uKCdhZnRlckNoYW5nZScsZnVuY3Rpb24oKXtcclxuICAgICAgICBvcGFjcGljKCk7XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgZnVuY3Rpb24gb3BhY3BpYygpe1xyXG4gICAgICAgIHZhciBiZWZvcmVpdGVtID0gLTE7XHJcbiAgICAgICAgJChcIi5zbGlja2Jsb2NrOSAuaXRlbVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtcclxuICAgICAgICAgICAgaWYoJChcIi5zbGlja2Jsb2NrOSAuaXRlbTplcShcIitpbmRleCtcIilcIikuYXR0cihcImFyaWEtaGlkZGVuXCIpPT1cImZhbHNlXCIpe1xyXG4gICAgICAgICAgICAgICAgYmVmb3JlaXRlbSA9IGluZGV4LTE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAkKFwiLnNsaWNrYmxvY2s5IC5pdGVtXCIpLnJlbW92ZUNsYXNzKFwib3BjcGljXCIpO1xyXG4gICAgICAgICQoXCIuc2xpY2tibG9jazkgLml0ZW06ZXEoXCIrYmVmb3JlaXRlbStcIilcIikuYWRkQ2xhc3MoXCJvcGNwaWNcIik7XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gb3BlbnBpYygpe1xyXG4gICAgICAgIHZhciBiZWZvcmVpdGVtID0gLTE7XHJcbiAgICAgICAgJChcIi5zbGlja2Jsb2NrOSAuaXRlbVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtcclxuICAgICAgICAgICAgaWYoJChcIi5zbGlja2Jsb2NrOSAuaXRlbTplcShcIitpbmRleCtcIilcIikuYXR0cihcImFyaWEtaGlkZGVuXCIpPT1cImZhbHNlXCIpe1xyXG4gICAgICAgICAgICAgICAgYmVmb3JlaXRlbSA9IGluZGV4LTE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAkKFwiLnNsaWNrYmxvY2s5IC5pdGVtOmVxKFwiK2JlZm9yZWl0ZW0rXCIpXCIpLnJlbW92ZUNsYXNzKFwib3BjcGljXCIpO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAkKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgb3BhY3BpYygpO1xyXG4gICAgICAgIG9wZW5waWMoKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5zbGlja2Jsb2NrMTEnKS5vbignYWZ0ZXJDaGFuZ2UnLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgZGVsdmxpbmUoKTtcclxuICAgIH0pO1xyXG4gICAgZnVuY3Rpb24gZGVsdmxpbmUoKXtcclxuICAgICAgICB2YXIgbGFzdGl0ZW0gPSAtMTtcclxuICAgICAgICB2YXIgZmlyc3RpdGVtID0gMDtcclxuICAgICAgICB2YXIgY291bnRpdGVtID0gMDtcclxuICAgICAgICAkKFwiLnNsaWNrYmxvY2sxMSAuaXRlbVwiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtcclxuICAgICAgICAgICAgaWYoJChcIi5zbGlja2Jsb2NrMTEgLml0ZW06ZXEoXCIraW5kZXgrXCIpXCIpLmF0dHIoXCJhcmlhLWhpZGRlblwiKT09XCJmYWxzZVwiKXtcclxuICAgICAgICAgICAgICAgIGNvdW50aXRlbSArPSAxO1xyXG4gICAgICAgICAgICAgICAgaWYoY291bnRpdGVtID09IDEpe1xyXG4gICAgICAgICAgICAgICAgICAgIGZpcnN0aXRlbSA9IGluZGV4LTE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsYXN0aXRlbSA9IGluZGV4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJChcIi5zbGlja2Jsb2NrMTEgLml0ZW1cIikucmVtb3ZlQ2xhc3MoXCJ2bGluZVwiKTtcclxuICAgICAgICAkKFwiLnNsaWNrYmxvY2sxMSAuaXRlbVwiKS5hZGRDbGFzcyhcInZsaW5lXCIpO1xyXG4gICAgICAgICQoXCIuc2xpY2tibG9jazExIC5pdGVtOmVxKFwiK2xhc3RpdGVtK1wiKVwiKS5yZW1vdmVDbGFzcyhcInZsaW5lXCIpO1xyXG4gICAgICAgICQoXCIuc2xpY2tibG9jazExIC5pdGVtOmVxKFwiK2ZpcnN0aXRlbStcIilcIikucmVtb3ZlQ2xhc3MoXCJ2bGluZVwiKTtcclxuICAgIH1cclxuICAgICQoZnVuY3Rpb24oKXtcclxuICAgICAgICBkZWx2bGluZSgpO1xyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIGZ1bmN0aW9uIG1vdmVzbGlja3R5cGUyKCl7XHJcbiAgICAgICAgJCgnLnByZXYtc2xpZGUnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQoJy5zbGlja2Jsb2NrMicpLnNsaWNrKCdzbGlja1ByZXYnKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKCcubmV4dC1zbGlkZScpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJCgnLnNsaWNrYmxvY2syJykuc2xpY2soJ3NsaWNrTmV4dCcpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKCcucHJldi1zbGlkZTEwJykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgJCgnLnNsaWNrYmxvY2sxMC1wcmV2aWV3Jykuc2xpY2soJ3NsaWNrUHJldicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQoJy5uZXh0LXNsaWRlMTAnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAkKCcuc2xpY2tibG9jazEwLXByZXZpZXcnKS5zbGljaygnc2xpY2tOZXh0Jyk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoJy5wcmV2LXNsaWRlMTEnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQoJy5zbGlja2Jsb2NrMTEnKS5zbGljaygnc2xpY2tQcmV2Jyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJCgnLm5leHQtc2xpZGUxMScpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJCgnLnNsaWNrYmxvY2sxMScpLnNsaWNrKCdzbGlja05leHQnKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJCgnLnByZXYtc2xpZGUxNicpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJCgnLnNsaWNrYmxvY2sxNicpLnNsaWNrKCdzbGlja1ByZXYnKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKCcubmV4dC1zbGlkZTE2Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAkKCcuc2xpY2tibG9jazE2Jykuc2xpY2soJ3NsaWNrTmV4dCcpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKCcucHJldi1zbGlkZTE5Jykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAkKCcuc2xpY2tibG9jazE5Jykuc2xpY2soJ3NsaWNrUHJldicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQoJy5uZXh0LXNsaWRlMTknKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQoJy5zbGlja2Jsb2NrMTknKS5zbGljaygnc2xpY2tOZXh0Jyk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoJy5wcmV2LXNsaWRlMjAnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICQoJy5zbGlja2Jsb2NrMjAnKS5zbGljaygnc2xpY2tQcmV2Jyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJCgnLm5leHQtc2xpZGUyMCcpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJCgnLnNsaWNrYmxvY2syMCcpLnNsaWNrKCdzbGlja05leHQnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn0pO1xyXG5cclxuXHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLWZvciBzbGlja2Jsb2NrMyAsIHNsaWNrYmxvY2sxOCAsIGFuZCBzbGlja2Jsb2NrMTkgKGdldG1heGhlaWdodDIgZm9yIHNsaWNrYmxvY2sxOSkgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qLyBcclxudmFyIHRtcGhpZ2ggPSAwO1xyXG52YXIgY3VycmVudGFjdGl2ZSA9IGZhbHNlO1xyXG52YXIgaXNJRTggPSBmYWxzZTtcclxuZnVuY3Rpb24gZ2V0bWF4aGVpZ2h0KCl7XHJcbiAgICB0bXBoaWdoID0gMDtcclxuICAgIGlmKCQod2luZG93KS53aWR0aCgpID4gNzY3KXtcclxuICAgICAgICAkKFwiLnBpY2ZyYW1lM1wiKS5lYWNoKGZ1bmN0aW9uKGluZGV4KXtcclxuICAgICAgICAgICAgaWYoJCh0aGlzKS5oZWlnaHQoKSA+IHRtcGhpZ2gpe1xyXG4gICAgICAgICAgICAgICAgdG1waGlnaCA9ICQodGhpcykuaGVpZ2h0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAkKFwiLnBpY2ZyYW1lM1wiKS5oZWlnaHQodG1waGlnaCk7XHJcbiAgICAgICAgaWYoaXNJRTgpe1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50YWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0sMjAwMCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRtYXhoZWlnaHQyKCl7XHJcbiAgICB0bXBoaWdoID0gMDtcclxuICAgIGlmKCQod2luZG93KS53aWR0aCgpID4gNzY3KXtcclxuICAgICAgICB0bXBoaWdoID0gJChcIi5zbGljay1jZW50ZXJcIikuaGVpZ2h0KCk7XHJcbiAgICAgICAgdG1waGlnaCA9IHRtcGhpZ2g7XHJcbiAgICAgICAgJChcIi5ib3h3cmFwXCIpLmhlaWdodCh0bXBoaWdoKTtcclxuICAgICAgICAkKFwiLnNsaWNrLWNlbnRlclwiKS5jc3MoJ2hlaWdodCcsJ2F1dG8nKTtcclxuICAgIH1lbHNle1xyXG4gICAgICAgICQoXCIuYm94d3JhcFwiKS5oZWlnaHQoJ2F1dG8nKTtcclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gcmVzaXplU2xpY2tJdGVtKGNoa2Rpdm5tKXtcclxuICAgIHZhciBzbGt3aWR0aCA9ICQoY2hrZGl2bm0pLndpZHRoKCk7XHJcbiAgICBpZigkKHdpbmRvdykud2lkdGgoKSA+IDc2Nyl7XHJcbiAgICAgICAgJCggY2hrZGl2bm0gKyBcIiAuaXRlbVwiICkuZWFjaChmdW5jdGlvbiggaW5kZXggKSB7XHJcbiAgICAgICAgICAgIGlmKCQod2luZG93KS53aWR0aCgpIDw9ICBwYXJzZUludCgkKGNoa2Rpdm5tKS5hdHRyKFwiZGF0YS1yZXNwLW1vYmlsZS1zaXplXCIpKSl7ICAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAkKHRoaXMpLndpZHRoKHNsa3dpZHRoKTtcdFx0XHRcdFxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICAkKHRoaXMpLndpZHRoKHBhcnNlSW50KCQodGhpcykuYXR0cihcImRhdGEtd2lkdGgtcGVyXCIpKSpzbGt3aWR0aC8xMDApOyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbiAkKGRvY3VtZW50KS5vbigncmVhZHknLCBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAkKFwiLnBpY2ZyYW1lM1wiKS5jc3MoJ2hlaWdodCcsJ2F1dG8nKTtcclxuICAgICQoXCIuYm94d3JhcFwiKS5jc3MoJ2hlaWdodCcsJ2F1dG8nKTtcclxuICAgIGlmKCQod2luZG93KS53aWR0aCgpID4gNzY3KXtcclxuICAgICAgICByZXNpemVTbGlja0l0ZW0oXCIuc2xpY2tibG9jazNcIik7XHJcbiAgICAgICAgcmVzaXplU2xpY2tJdGVtKFwiLnNsaWNrYmxvY2sxOFwiKTsgICAgIFxyXG4gICAgICAgIGdldG1heGhlaWdodCgpOyAgXHJcbiAgICAgICAgZ2V0bWF4aGVpZ2h0MigpOyAgXHJcbiAgICB9ICBcclxuIH0pO1xyXG5cclxuICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKXtcclxuICAgIC8qLS0tLS0gRm9yIElFOCAtLS0tLSovXHJcbiAgICBpZigkLmJyb3dzZXIubXNpZSAmJiBwYXJzZUZsb2F0KCQuYnJvd3Nlci52ZXJzaW9uKTw9OCl7XHJcbiAgICAgICAgaXNJRTggPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBpZighY3VycmVudGFjdGl2ZSl7XHJcbiAgICAgICAgaWYoaXNJRTgpe1xyXG4gICAgICAgICAgICBjdXJyZW50YWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrMyBsaTplcSgwKSBidXR0b24nKS5jbGljaygpO1xyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrMTggbGk6ZXEoMCkgYnV0dG9uJykuY2xpY2soKTsgXHJcbiAgICAgICAgdG1waGlnaCA9IDA7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAkKFwiLnBpY2ZyYW1lM1wiKS5jc3MoJ2hlaWdodCcsJ2F1dG8nKTtcclxuICAgICAgICAgICAgJChcIi5ib3h3cmFwXCIpLmNzcygnaGVpZ2h0JywnYXV0bycpO1xyXG4gICAgICAgICAgICBpZigkKHdpbmRvdykud2lkdGgoKSA+IDc2Nyl7XHJcbiAgICAgICAgICAgICAgICByZXNpemVTbGlja0l0ZW0oXCIuc2xpY2tibG9jazNcIik7ICAgXHJcbiAgICAgICAgICAgICAgICByZXNpemVTbGlja0l0ZW0oXCIuc2xpY2tibG9jazE4XCIpO1xyXG4gICAgICAgICAgICAgICAgZ2V0bWF4aGVpZ2h0KCk7XHJcbiAgICAgICAgICAgICAgICBnZXRtYXhoZWlnaHQyKCk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgJChcIi5ib3h3cmFwXCIpLmhlaWdodCgnYXV0bycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwxMDApO1xyXG4gICAgfVxyXG4gICBcclxufSk7XHJcblxyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmZ1bmN0aW9uIGluaXRmYXFibG9jaygpe1xyXG4gICAgJCgnLm1haW5mYXFpbmMgLnF1ZXN0aW9uY2xpY2snKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgJCh0aGlzKS5wYXJlbnQoKS5maW5kKCcuY2F0YW5zd2VyJykuc2xpZGVUb2dnbGUoMzAwLGZ1bmN0aW9uKCl7fSk7XHJcbiAgICB9KVxyXG59XHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1jaGFnbmUgZHJvcGRvd24gY3VycmVuY3ktLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmZ1bmN0aW9uIGNoYW5nZWN1cnJlbmN5KCl7XHJcbiAgICB2YXIgY3VySXRlbSA9ICQoJy5kcm9wZG93bi1tZW51IGxpJyk7XHJcbiAgICBjdXJJdGVtLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgIGlmKCQoJy5kZlZhbCcpLmxlbmd0aD09MCl7XHJcbiAgICAgICAgICAgICQoJy5kcm9wZG93bi1tZW51JykucHJlcGVuZCgkKCc8bGkgY2xhc3M9XCJkZlZhbFwiPjxhIGhyZWY9XCIjXCI+4LiX4Li44LiB4Liq4LiB4Li44Lil4LmA4LiH4Li04LiZPC9hPjwvbGk+JykpO1xyXG4gICAgICAgICAgICAkKCcuZGZWYWwnKS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAkKCcjZGVmYXVsdFZhbCcpLmh0bWwoXCLguJfguLjguIHguKrguIHguLjguKXguYDguIfguLTguJlcIik7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgJCgnI2N1cnJlbmN5VmFsJykudmFsKFwi4LiX4Li44LiB4Liq4LiB4Li44Lil4LmA4LiH4Li04LiZXCIpO1xyXG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZygkKCcjY3VycmVuY3lWYWwnKS52YWwoKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB2YXIgY3VyVmFsID0gJCh0aGlzKS50ZXh0KCk7XHJcbiAgICAgICAgJCgnI2N1cnJlbmN5VmFsJykudmFsKGN1clZhbCk7XHJcbiAgICAgICAgdmFyIGh0bWxTdHIgPSAkKHRoaXMpLmNoaWxkcmVuKCkuaHRtbCgpO1xyXG4gICAgICAgICQoJyNkZWZhdWx0VmFsJykuaHRtbChodG1sU3RyKTtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKCQoJyNjdXJyZW5jeVZhbCcpLnZhbCgpKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gQ2xvc2UgQnV0dG9uIExpdHkgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG5mdW5jdGlvbiBjbG9zZWxpdHlidG4oKXtcclxuICAgICQoJy5saXR5LWNsb3NlJykuc2hvdygpO1xyXG4gICAgJCgnLmxpdHktY2xvc2UnKS5odG1sKCcnKTtcclxufVxyXG5cclxuLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBuZXcgc2NyaXB0IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy9cclxuLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBzbGljayB0ZW1wbGF0ZSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS8vXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tIGZvciBzbGlja3JldjEyLCBzbGlja3JldjEzIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS8vXHJcbnZhciBzbGlja2Jsb2NrcmV2MTNfc2V0dGluZyA9IHtcclxuICAgIGFycm93czpmYWxzZSxcclxuICAgIGRvdHM6dHJ1ZSxcclxuICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgcmVzcG9uc2l2ZTpbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojc2NyxcclxuICAgICAgICAgICAgc2xpZGVzVG9TaG93OjEsXHJcbiAgICAgICAgICAgIHN3aXBlOnRydWUsXHJcbiAgICAgICAgfSxcclxuICAgIF0sXHJcbn1cclxudmFyIHNsaWNrYmxvY2tyZXYxMl9zZXR0aW5nID0ge1xyXG4gICAgc2xpZGVzVG9TaG93OjYsXHJcbiAgICBpbmZpbml0ZTpmYWxzZSxcclxuICAgIGFycm93czp0cnVlLFxyXG4gICAgZG90czpmYWxzZSxcclxuICAgIHNsaWRlc1RvU2Nyb2xsOiAzLFxyXG4gICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgcmVzcG9uc2l2ZTpbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojk5MSxcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiA0LFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY3LFxyXG4gICAgICAgICAgICBzZXR0aW5nczogXCJ1bnNsaWNrXCIsXHJcbiAgICAgICAgfSxcclxuICAgIF0sXHJcbn1cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLSBlbmQgZm9yIHNsaWNrcmV2MTIsIHNsaWNrcmV2MTMgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0gZm9yIHVuc2xpY2sgMiBkZXZpY2UgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy9cclxudmFyIHdpbmRvd09sZDtcclxudmFyIHdpbmRvd1VwZGF0ZTtcclxuXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XHJcbiAgICB3aW5kb3dVcGRhdGUgPSAkKHdpbmRvdykud2lkdGgoKTtcclxuICAgIGlmKCQod2luZG93KS53aWR0aCgpPjc2Nyl7XHJcbiAgICAgICAgc2xpY2tibG9ja3JldjEyKCk7XHJcbiAgICAgICAgd2luZG93T2xkID0gNzY4O1xyXG4gICAgfWVsc2V7XHJcbiAgICAgICAgc2xpY2tibG9ja3JldjEzKCk7XHJcbiAgICAgICAgd2luZG93T2xkID0gNzY3O1xyXG4gICAgfVxyXG59KTtcclxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe1xyXG4gICAgd2luZG93VXBkYXRlID0gJCh3aW5kb3cpLndpZHRoKCk7XHJcbiAgICAvL3Vuc2xpY2sgZGVza3RvcFxyXG4gICAgaWYoIHdpbmRvd09sZCA+IDc2NyAmJiB3aW5kb3dVcGRhdGUgPD0gNzY3ICYmICEkKCcuc2xpY2tibG9ja3JldjEzJykuaGFzQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJykpe1xyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrcmV2MTMnKS5zbGljayhzbGlja2Jsb2NrcmV2MTNfc2V0dGluZyk7XHJcbiAgICB9ZWxzZSBpZih3aW5kb3dVcGRhdGUgPiA3NjcgJiYgJCgnLnNsaWNrYmxvY2tyZXYxMycpLmhhc0NsYXNzKCdzbGljay1pbml0aWFsaXplZCcpKXtcclxuICAgICAgICAvL3dpbmRvdz43NjdcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKCcuc2xpY2tibG9ja3JldjEzJykuc2xpY2soJ3Vuc2xpY2snKTtcclxuICAgICAgICB9LCA1MDApO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgLy91bnNsaWNrIG1vYmlsZVxyXG4gICAgaWYoIHdpbmRvd09sZCA8PTc2NyAmJiB3aW5kb3dVcGRhdGUgPiA3Njcpe1xyXG4gICAgICAgIC8vd2luZG93Pjc2N1xyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrcmV2MTInKS5zbGljayhzbGlja2Jsb2NrcmV2MTJfc2V0dGluZyk7XHJcbiAgICAgICAgXHJcbiAgICB9ZWxzZSBpZiggd2luZG93VXBkYXRlIDw9IDc2NyAmJiAkKCcuc2xpY2tibG9ja3JldjEyJykuaGFzQ2xhc3MoJ3NsaWNrLWluaXRpYWxpemVkJykpe1xyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrcmV2MTInKS5zbGljaygndW5zbGljaycpO1xyXG4gICAgfVxyXG4gICAgd2luZG93T2xkID0gd2luZG93VXBkYXRlO1xyXG59KTtcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLSBlbmQgZm9yIHVuc2xpY2sgMiBkZXZpY2UgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy9cclxuXHJcbiAkKGZ1bmN0aW9uKCl7XHJcbiAgICBzbGlja2Jsb2NrcmV2MSgpO1xyXG4gICAgc2xpY2tibG9ja3JldjIoKTtcclxuICAgIHNsaWNrYmxvY2tyZXYzKCk7XHJcbiAgICBzbGlja2Jsb2NrcmV2NCgpO1xyXG4gICAgc2xpY2tibG9ja3JldjUoKTtcclxuICAgIHNsaWNrYmxvY2tyZXY2KCk7XHJcbiAgICBzbGlja2Jsb2NrcmV2NygpO1xyXG4gICAgc2xpY2tibG9ja3JldjgoKTtcclxuICAgIHNsaWNrYmxvY2tyZXY5KCk7XHJcbiAgICBzbGlja2Jsb2NrcmV2MTAoKTtcclxuICAgIHNsaWNrYmxvY2tyZXYxMSgpO1xyXG4gICAgc2xpY2tibG9ja3JldjE0KCk7XHJcbiAgICBkZXRlY3RGaXJlZm94KCk7XHJcbiB9KTtcclxuZnVuY3Rpb24gc2xpY2tibG9ja3JldjEoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2MScpLnNsaWNrKHtcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICBmYWRlOiB0cnVlLFxyXG4gICAgICAgIHNwZWVkOiAyMDAwLFxyXG4gICAgICAgIHRvdWNoVGhyZXNob2xkOiAxMDAsXHJcbiAgICAgICAgd2FpdEZvckFuaW1hdGU6IHRydWUsXHJcbiAgICAgICAgLy8gYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgLy8gYXV0b3BsYXlTcGVlZDogNzAwMCxcclxuICAgICAgICAvLyBwYXVzZU9uRm9jdXM6IGZhbHNlLCBcclxuICAgICAgICAvLyBwYXVzZU9uSG92ZXI6IGZhbHNlXHJcbiAgICB9KTtcclxuICAgICQoXCIuc2xpY2tibG9ja3JldjEgLnNsaWNrLWRvdHNcIikud3JhcChcIjxkaXYgY2xhc3M9XFxcInNsaWNrLWRvdHMtd3JhcFxcXCI+PC9kaXY+XCIpO1xyXG4gICAgLy8gJCgnLnNsaWNrYmxvY2tyZXYxIC5iYWNrZ3JvdW5kLWNoZXZyb24sIC5zbGlja2Jsb2NrcmV2MSAuY2hldnJvbi1vdmVybGF5LWRlZmF1bHQsLnNsaWNrYmxvY2tyZXYxIC5mYWRlSW5Eb3duJykuYWRkQ2xhc3MoJ2FuaW1hdGVkJyk7XHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLypjbGVhciBpbml0aWFsKi9cclxuICAgICAgICAkKCcuc2xpY2tibG9ja3JldjEgLmJhY2tncm91bmQtY2hldnJvbiwgLnNsaWNrYmxvY2tyZXYxIC5jaGV2cm9uLW92ZXJsYXktZGVmYXVsdCcpLnJlbW92ZUNsYXNzKCdmYWRlSW5Eb3duSW1nIGZhZGVJbiBhbmltYXRlZCcpO1xyXG4gICAgfSwgMjAwMCk7XHJcbiAgICAkKCcuc2xpY2tibG9ja3JldjEnKS5vbignYmVmb3JlQ2hhbmdlJywgZnVuY3Rpb24oZXZlbnQsIHNsaWNrLCBjdXJyZW50U2xpZGUsIG5leHRTbGlkZSl7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIGN1cnJlbnRFbG0gPSAnLnNsaWNrYmxvY2tyZXYxIC5pdGVtW2RhdGEtc2xpY2staW5kZXg9JytjdXJyZW50U2xpZGUrJ10gJztcclxuICAgICAgICB2YXIgbmV4dEVsbSA9ICcuc2xpY2tibG9ja3JldjEgLml0ZW1bZGF0YS1zbGljay1pbmRleD0nK25leHRTbGlkZSsnXSAnO1xyXG4gICAgICAgIHZhciBFbG0gPSAnLnNsaWNrYmxvY2tyZXYxICc7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAkKGN1cnJlbnRFbG0rJy50ZXh0YW5pbWF0ZSAuaGR0eHQnKS5yZW1vdmVDbGFzcygnZmFkZUluRG93biBhbmltYXRlZCcpLmFkZENsYXNzKCdmYWRlT3V0RG93biBhbmltYXRlZCcpO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICAkKGN1cnJlbnRFbG0rJy50ZXh0YW5pbWF0ZSAudGV4dCcpLnJlbW92ZUNsYXNzKCdmYWRlSW5Eb3duIGFuaW1hdGVkJykuYWRkQ2xhc3MoJ2ZhZGVPdXREb3duIGFuaW1hdGVkJyk7XHJcbiAgICAgICAgICAgICQoY3VycmVudEVsbSsnLnRleHRhbmltYXRlIC5idXR0b25ibG9jaycpLnJlbW92ZUNsYXNzKCdmYWRlSW5Eb3duIGFuaW1hdGVkJykuYWRkQ2xhc3MoJ2ZhZGVPdXREb3duIGFuaW1hdGVkJyk7XHJcbiAgICAgICAgICAgICQoY3VycmVudEVsbSsnLnBpY2Jsb2NrMS5oaWRkZW4teHMnKS5yZW1vdmVDbGFzcygnZmFkZUluRG93bkltZyBhbmltYXRlZCcpLmFkZENsYXNzKCdmYWRlT3V0RG93bkltZyBhbmltYXRlZCcpO1xyXG4gICAgICAgIC8vIH0sIDUwKTtcclxuICAgICAgICBcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIC8qY2xlYXIgb2xkIGNsYXNzKi9cclxuICAgICAgICAgICAgJCgnLnNsaWNrYmxvY2tyZXYxIC5jaGV2cm9uLW92ZXJsYXktZGVmYXVsdCcpLnJlbW92ZUNsYXNzKCdjaGV2cm9uLW92ZXJsYXktZW50ZXIgY2hldnJvbi1vdmVybGF5LWxlYXZlJyk7XHJcbiAgICAgICAgfSwgODApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJChFbG0rJy5jaGV2cm9uLW92ZXJsYXktZGVmYXVsdCcpLmFkZENsYXNzKCdjaGV2cm9uLW92ZXJsYXktZW50ZXInKTtcclxuICAgICAgICAgICAgJChjdXJyZW50RWxtKycuY2hldnJvbi1vdmVybGF5LWRlZmF1bHQnKS5vbignYW5pbWF0aW9uZW5kIHdlYmtpdEFuaW1hdGlvbkVuZCBvQW5pbWF0aW9uRW5kJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgJChFbG0rJy5jaGV2cm9uLW92ZXJsYXktZGVmYXVsdCcpLmRlbGF5KCcxMDAnKS5hZGRDbGFzcygnY2hldnJvbi1vdmVybGF5LWxlYXZlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgJChuZXh0RWxtKycudGV4dGFuaW1hdGUgLmhkdHh0JykucmVtb3ZlQ2xhc3MoJ2ZhZGVPdXREb3duIGFuaW1hdGVkJykuYWRkQ2xhc3MoJ2ZhZGVJbkRvd24gYW5pbWF0ZWQnKTtcclxuICAgICAgICAgICAgJChuZXh0RWxtKycudGV4dGFuaW1hdGUgLnRleHQnKS5yZW1vdmVDbGFzcygnZmFkZU91dERvd24gYW5pbWF0ZWQnKS5hZGRDbGFzcygnZmFkZUluRG93biBhbmltYXRlZCcpO1xyXG4gICAgICAgICAgICAkKG5leHRFbG0rJy50ZXh0YW5pbWF0ZSAuYnV0dG9uYmxvY2snKS5yZW1vdmVDbGFzcygnZmFkZU91dERvd24gYW5pbWF0ZWQnKS5hZGRDbGFzcygnZmFkZUluRG93biBhbmltYXRlZCcpO1xyXG4gICAgICAgICAgICAkKG5leHRFbG0rJy5waWNibG9jazEuaGlkZGVuLXhzJykucmVtb3ZlQ2xhc3MoJ2ZhZGVPdXREb3duSW1nIGFuaW1hdGVkJykuYWRkQ2xhc3MoJ2ZhZGVJbkRvd25JbWcgYW5pbWF0ZWQnKTtcclxuICAgICAgICB9LCAyMDApO1xyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfSk7XHJcbn1cclxuZnVuY3Rpb24gc2xpY2tibG9ja3JldjIoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2MicpLnNsaWNrKHtcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDMsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgICBwcmV2QXJyb3c6ICc8YSBjbGFzcz1cInByZXYtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICBuZXh0QXJyb3c6ICc8YSBjbGFzcz1cIm5leHQtc2xpZGVcIj48L2E+JyxcclxuICAgIH0pO1xyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgIC8qY2xlYXIgaW5pdGlhbCovXHJcbiAgICAgICAgJCgnLnNsaWNrYmxvY2tyZXYxIC5iYWNrZ3JvdW5kLWNoZXZyb24sIC5zbGlja2Jsb2NrcmV2MSAuY2hldnJvbi1vdmVybGF5LWRlZmF1bHQnKS5yZW1vdmVDbGFzcygnZmFkZUluRG93bkltZyBmYWRlSW4gYW5pbWF0ZWQnKTtcclxuICAgIH0sIDIwMDApO1xyXG4gICAgLyogbmF2ICovXHJcbiAgICAvLyAkKCcucHJldi1zbGlkZS0yJykub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgLy8gICAgICQoJy5zbGlja2Jsb2NrcmV2MicpLnNsaWNrKCdzbGlja1ByZXYnKTtcclxuICAgIC8vIH0pO1xyXG4gICAgLy8gJCgnLm5leHQtc2xpZGUtMicpLm9uKCdjbGljaycsZnVuY3Rpb24oKXtcclxuICAgIC8vICAgICAkKCcuc2xpY2tibG9ja3JldjInKS5zbGljaygnc2xpY2tOZXh0Jyk7XHJcbiAgICAvLyB9KTtcclxufVxyXG5mdW5jdGlvbiBzbGlja2Jsb2NrcmV2Mygpe1xyXG4gICAgJCgnLnNsaWNrYmxvY2tyZXYzJykuc2xpY2soe1xyXG4gICAgICAgIGluaXRpYWxTbGlkZTogMCxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6MixcclxuICAgICAgICBhZGFwdGl2ZUhlaWdodDogdHJ1ZSxcclxuXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgICBwcmV2QXJyb3c6ICc8YSBjbGFzcz1cInByZXYtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICBuZXh0QXJyb3c6ICc8YSBjbGFzcz1cIm5leHQtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFt7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY3LFxyXG4gICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MSxcclxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOjEsXHJcbiAgICAgICAgICAgICAgICAvLyBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHN3aXBlOnRydWUsXHJcbiAgICAgICAgICAgICAgICBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgYWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxdXHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyAkKCcuc2xpY2tibG9ja3JldjMnKS5zbGljaygnc2xpY2tHb1RvJywgMSwgdHJ1ZSk7XHJcbn1cclxuZnVuY3Rpb24gc2xpY2tibG9ja3JldjQoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2NCcpLnNsaWNrKHtcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDQsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgYXJyb3dzOiB0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuICAgICAgICBwcmV2QXJyb3c6ICc8YSBjbGFzcz1cInByZXYtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICBuZXh0QXJyb3c6ICc8YSBjbGFzcz1cIm5leHQtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFsgICBcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDoxMjAwLFxyXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDo3NjcsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLjUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXVxyXG4gICAgfSk7XHJcbn1cclxuZnVuY3Rpb24gc2xpY2tibG9ja3JldjUoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2NScpLnNsaWNrKHtcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDMuNSxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMyxcclxuICAgICAgICBkb3RzOiB0cnVlLFxyXG4gICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgIHRvdWNoVGhyZXNob2xkOiA1MCxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDo5OTIsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLjUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICBicmVha3BvaW50Ojc2NyxcclxuICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLjIsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICAgICAgICAgIHN3aXBlOnRydWUsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LF1cclxuICAgIH0pO1xyXG59XHJcbmZ1bmN0aW9uIHNsaWNrYmxvY2tyZXY2KCl7XHJcbiAgICB2YXIgc2xpY2tibG9ja3JldjYgPSAkKCcuc2xpY2tibG9ja3JldjYnKTtcclxuICAgIC8vaW5pdGlhbCBiYWNrZ3JvdW5kIGltYWdlc1xyXG4gICAgaWYoJCh3aW5kb3cpLndpZHRoKCk+NzY3KXtcclxuICAgICAgICB2YXIgaW5pdGlhbHNyYyA9ICQoJy5zbGlja2Jsb2NrcmV2NiAucGljYmxvY2sxLmhpZGRlbi14cyBpbWcnKS5hdHRyKCdzcmMnKTtcclxuICAgIH1lbHNle1xyXG4gICAgICAgIHZhciBpbml0aWFsc3JjID0gJCgnLnNsaWNrYmxvY2tyZXY2IC5waWNibG9jazEudmlzaWJsZS14cyBpbWcnKS5hdHRyKCdzcmMnKTtcclxuICAgIH1cclxuXHJcbiAgICAkKCcuc2xpY2tibG9ja3JldjZfYmFja2dyb3VuZCcpLmNzcygnYmFja2dyb3VuZC1pbWFnZScsICd1cmwoJyArIGluaXRpYWxzcmMgKyAnKScpO1xyXG4gICAgXHJcbiAgICBzbGlja2Jsb2NrcmV2Ni5vbignaW5pdCcsIGZ1bmN0aW9uKGV2ZW50LCBzbGljaywgY3VycmVudFNsaWRlKSB7XHJcbiAgICAgICAgdmFyXHJcbiAgICAgICAgY3VyID0gJChzbGljay4kc2xpZGVzW3NsaWNrLmN1cnJlbnRTbGlkZV0pLFxyXG4gICAgICAgIG5leHQgPSBjdXIubmV4dCgpLFxyXG4gICAgICAgIHByZXYgPSBjdXIucHJldigpO1xyXG4gICAgICAgIHByZXYuYWRkQ2xhc3MoJ3NsaWNrLXNwcmV2Jyk7XHJcbiAgICAgICAgbmV4dC5hZGRDbGFzcygnc2xpY2stc25leHQnKTtcclxuICAgICAgICBjdXIucmVtb3ZlQ2xhc3MoJ3NsaWNrLXNuZXh0JykucmVtb3ZlQ2xhc3MoJ3NsaWNrLXNwcmV2Jyk7XHJcbiAgICAgICAgc2xpY2suJHByZXYgPSBwcmV2O1xyXG4gICAgICAgIHNsaWNrLiRuZXh0ID0gbmV4dDtcclxuICAgIH0pLm9uKCdiZWZvcmVDaGFuZ2UnLCBmdW5jdGlvbihldmVudCwgc2xpY2ssIGN1cnJlbnRTbGlkZSwgbmV4dFNsaWRlKSB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ2JlZm9yZUNoYW5nZScpO1xyXG4gICAgICAgIHZhclxyXG4gICAgICAgIGN1ciA9ICQoc2xpY2suJHNsaWRlc1tuZXh0U2xpZGVdKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhzbGljay4kcHJldiwgc2xpY2suJG5leHQpO1xyXG4gICAgICAgIHNsaWNrLiRwcmV2LnJlbW92ZUNsYXNzKCdzbGljay1zcHJldicpO1xyXG4gICAgICAgIHNsaWNrLiRuZXh0LnJlbW92ZUNsYXNzKCdzbGljay1zbmV4dCcpO1xyXG4gICAgICAgIG5leHQgPSBjdXIubmV4dCgpLFxyXG4gICAgICAgIHByZXYgPSBjdXIucHJldigpO1xyXG4gICAgICAgIHByZXYucHJldigpO1xyXG4gICAgICAgIHByZXYubmV4dCgpO1xyXG4gICAgICAgIHByZXYuYWRkQ2xhc3MoJ3NsaWNrLXNwcmV2Jyk7XHJcbiAgICAgICAgbmV4dC5hZGRDbGFzcygnc2xpY2stc25leHQnKTtcclxuICAgICAgICBzbGljay4kcHJldiA9IHByZXY7XHJcbiAgICAgICAgc2xpY2suJG5leHQgPSBuZXh0O1xyXG4gICAgICAgIGN1ci5yZW1vdmVDbGFzcygnc2xpY2stbmV4dCcpLnJlbW92ZUNsYXNzKCdzbGljay1zcHJldicpO1xyXG5cclxuICAgICAgICAvL2NoYW5nZSBiYWNrZ3JvdW5kIGFmdGVyIHN3aXBlXHJcbiAgICAgICAgdmFyIG5leHRFbG0gPSAnLnNsaWNrYmxvY2tyZXY2IC5pdGVtW2RhdGEtc2xpY2staW5kZXg9JytuZXh0U2xpZGUrJ10gJztcclxuICAgICAgICBpZigkKHdpbmRvdykud2lkdGgoKT43Njcpe1xyXG4gICAgICAgICAgICB2YXIgaW5pdGlhbHNyYyA9ICQobmV4dEVsbSsnIC5waWNibG9jazEuaGlkZGVuLXhzIGltZycpLmF0dHIoJ3NyYycpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICB2YXIgaW5pdGlhbHNyYyA9ICQobmV4dEVsbSsnIC5waWNibG9jazEudmlzaWJsZS14cyBpbWcnKS5hdHRyKCdzcmMnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgJCgnLnNsaWNrYmxvY2tyZXY2X2JhY2tncm91bmQnKS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCAndXJsKCcgKyBpbml0aWFsc3JjICsgJyknKTtcclxuICAgIH0pO1xyXG4gICAgXHJcbiAgICBzbGlja2Jsb2NrcmV2Ni5zbGljayh7XHJcbiAgICAgICAgc3BlZWQ6IDEwMDAsXHJcbiAgICAgICAgLy8gYXJyb3dzOiB0cnVlLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgZm9jdXNPblNlbGVjdDogdHJ1ZSxcclxuICAgICAgICAvLyBwcmV2QXJyb3c6ICc8YnV0dG9uPiBwcmV2PC9idXR0b24+JyxcclxuICAgICAgICAvLyBuZXh0QXJyb3c6ICc8YnV0dG9uPiBuZXh0PC9idXR0b24+JyxcclxuICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgICAgIHNsaWRlc1BlclJvdzogMSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgY2VudGVyUGFkZGluZzogJzAnLFxyXG4gICAgICAgIHN3aXBlOiB0cnVlLFxyXG4gICAgICAgIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgICAgIGF1dG9wbGF5U3BlZWQ6IDYwMDAsXHJcbiAgICAgICAgcGF1c2VPbkZvY3VzOiBmYWxzZSwgXHJcbiAgICAgICAgcGF1c2VPbkhvdmVyOiBmYWxzZSxcclxuICAgICAgICAvKmluZmluaXRlOiBmYWxzZSwqL1xyXG4gICAgfSk7XHJcbn1cclxuZnVuY3Rpb24gc2xpY2tibG9ja3JldjcoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2NycpLnNsaWNrKHtcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDIsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgYXJyb3dzOiB0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuICAgICAgICBwcmV2QXJyb3c6ICc8YSBjbGFzcz1cInByZXYtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICBuZXh0QXJyb3c6ICc8YSBjbGFzcz1cIm5leHQtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICAvLyBsYXp5TG9hZDogJ29uZGVtYW5kJyxcclxuICAgICAgICByZXNwb25zaXZlOlxyXG4gICAgICAgIFsgICBcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDoxMjAwLFxyXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDo5OTIsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLjUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6NjAwLFxyXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6e1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMS4xLFxyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgIHN3aXBlOnRydWUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF1cclxuICAgIH0pO1xyXG59XHJcbmZ1bmN0aW9uIHNsaWNrYmxvY2tyZXY4KCl7XHJcbiAgICAkKCcuc2xpY2tibG9ja3JldjgnKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgc3BlZWQ6IDEwMDAsXHJcbiAgICAgICAgd2FpdEZvckFuaW1hdGU6IHRydWUsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDIwLFxyXG4gICAgICAgIGZhZGU6IHRydWUsXHJcbiAgICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgYXV0b3BsYXlTcGVlZDogNTAwMCxcclxuICAgICAgICBwYXVzZU9uRm9jdXM6IGZhbHNlLCBcclxuICAgICAgICBwYXVzZU9uSG92ZXI6IGZhbHNlXHJcbiAgICB9KTtcclxuICAgICQoXCIuc2xpY2tibG9ja3JldjggLnNsaWNrLWRvdHNcIikud3JhcChcIjxkaXYgY2xhc3M9XFxcInNsaWNrLWRvdHMtd3JhcFxcXCI+PC9kaXY+XCIpO1xyXG5cclxuICAgICQoJy5zbGlja2Jsb2NrcmV2OCcpLm9uKCdiZWZvcmVDaGFuZ2UnLCBmdW5jdGlvbihldmVudCwgc2xpY2ssIGN1cnJlbnRTbGlkZSwgbmV4dFNsaWRlKXtcclxuICAgICAgICB2YXIgY3VycmVudEVsbSA9ICcuc2xpY2tibG9ja3JldjggLml0ZW1bZGF0YS1zbGljay1pbmRleD0nK2N1cnJlbnRTbGlkZSsnXSAnO1xyXG4gICAgICAgIHZhciBuZXh0RWxtID0gJy5zbGlja2Jsb2NrcmV2OCAuaXRlbVtkYXRhLXNsaWNrLWluZGV4PScrbmV4dFNsaWRlKyddICc7XHJcbiAgICAgICAgJChjdXJyZW50RWxtKycuYmxvY2t0aXRsZSAuaGR0eHQnKS5yZW1vdmVDbGFzcygnZmFkZUluRG93biBhbmltYXRlZCcpLmFkZENsYXNzKCdmYWRlT3V0RG93biBhbmltYXRlZCcpO1xyXG4gICAgICAgICQoY3VycmVudEVsbSsnLmJsb2NrdGl0bGUgLnRleHQnKS5yZW1vdmVDbGFzcygnZmFkZUluRG93biBhbmltYXRlZCcpLmFkZENsYXNzKCdmYWRlT3V0RG93biBhbmltYXRlZCcpO1xyXG4gICAgICAgICQoY3VycmVudEVsbSsnLmJsb2NrdGl0bGUgLmJ1dHRvbmJsb2NrJykucmVtb3ZlQ2xhc3MoJ2ZhZGVJbkRvd24gYW5pbWF0ZWQnKS5hZGRDbGFzcygnZmFkZU91dERvd24gYW5pbWF0ZWQnKTtcclxuICAgICAgICAkKGN1cnJlbnRFbG0rJy5ibG9ja3RpdGxlIC5idG5jaXJjbGUtYXJyb3cnKS5yZW1vdmVDbGFzcygnZmFkZUluIGFuaW1hdGVkJykuYWRkQ2xhc3MoJ2ZhZGVPdXQgYW5pbWF0ZWQnKTtcclxuICAgICAgICBcclxuICAgICAgICAkKG5leHRFbG0rJy5ibG9ja3RpdGxlIC5oZHR4dCcpLnJlbW92ZUNsYXNzKCdmYWRlT3V0RG93biBhbmltYXRlZCcpLmFkZENsYXNzKCdmYWRlSW5Eb3duIGFuaW1hdGVkJyk7XHJcbiAgICAgICAgJChuZXh0RWxtKycuYmxvY2t0aXRsZSAudGV4dCcpLnJlbW92ZUNsYXNzKCdmYWRlT3V0RG93biBhbmltYXRlZCcpLmFkZENsYXNzKCdmYWRlSW5Eb3duIGFuaW1hdGVkJyk7XHJcbiAgICAgICAgJChuZXh0RWxtKycuYmxvY2t0aXRsZSAuYnV0dG9uYmxvY2snKS5yZW1vdmVDbGFzcygnZmFkZU91dERvd24gYW5pbWF0ZWQnKS5hZGRDbGFzcygnZmFkZUluRG93biBhbmltYXRlZCcpO1xyXG4gICAgICAgICQobmV4dEVsbSsnLmJsb2NrdGl0bGUgLmJ0bmNpcmNsZS1hcnJvdycpLnJlbW92ZUNsYXNzKCdmYWRlT3V0IGFuaW1hdGVkJykuYWRkQ2xhc3MoJ2ZhZGVJbiBhbmltYXRlZCcpO1xyXG5cclxuICAgIH0pO1xyXG59XHJcbmZ1bmN0aW9uIHNsaWNrYmxvY2tyZXY5KCl7XHJcbiAgICAkKCcuc2xpY2tibG9ja3JldjknKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDIwLFxyXG4gICAgICAgIGZhZGU6IHRydWUsXHJcbiAgICAgICAgcHJldkFycm93OiAnPGEgY2xhc3M9XCJwcmV2LXNsaWRlXCI+PC9hPicsXHJcbiAgICAgICAgbmV4dEFycm93OiAnPGEgY2xhc3M9XCJuZXh0LXNsaWRlXCI+PC9hPicsXHJcbiAgICB9KTtcclxufVxyXG5mdW5jdGlvbiBzbGlja2Jsb2NrcmV2MTAoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2MTAnKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgICBkb3RzOiB0cnVlLFxyXG4gICAgICAgIHRvdWNoVGhyZXNob2xkOiAyMCxcclxuICAgICAgICBmYWRlOiB0cnVlLFxyXG4gICAgICAgIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgICAgIGF1dG9wbGF5U3BlZWQ6IDQ1MDAsXHJcbiAgICAgICAgcGF1c2VPbkZvY3VzOiBmYWxzZSwgXHJcbiAgICAgICAgcGF1c2VPbkhvdmVyOiBmYWxzZSxcclxuICAgICAgICBwcmV2QXJyb3c6ICc8YSBjbGFzcz1cInByZXYtc2xpZGVcIj48L2E+JyxcclxuICAgICAgICBuZXh0QXJyb3c6ICc8YSBjbGFzcz1cIm5leHQtc2xpZGVcIj48L2E+JyxcclxuICAgIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzbGlja2Jsb2NrcmV2MTEoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2MTEnKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAyLjUsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDIsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgICAgIGRvdHM6IHRydWUsXHJcbiAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgcHJldkFycm93OiAnPGEgY2xhc3M9XCJwcmV2LXNsaWRlXCI+PC9hPicsXHJcbiAgICAgICAgbmV4dEFycm93OiAnPGEgY2xhc3M9XCJuZXh0LXNsaWRlXCI+PC9hPicsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbICAgXHJcbiAgICAgICAgICBcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDo5OTEsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLjUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXVxyXG4gICAgfSk7XHJcbn1cclxuZnVuY3Rpb24gc2xpY2tibG9ja3JldjEyKCl7XHJcbiAgICAkKCcuc2xpY2tibG9ja3JldjEyJykuc2xpY2soe1xyXG4gICAgICAgIHNsaWRlc1RvU2hvdzo2LFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIGFycm93czp0cnVlLFxyXG4gICAgICAgIGRvdHM6ZmFsc2UsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDMsXHJcbiAgICAgICAgdG91Y2hUaHJlc2hvbGQ6IDUwLFxyXG4gICAgICAgIHByZXZBcnJvdzogJzxhIGNsYXNzPVwicHJldi1zbGlkZVwiPjwvYT4nLFxyXG4gICAgICAgIG5leHRBcnJvdzogJzxhIGNsYXNzPVwibmV4dC1zbGlkZVwiPjwvYT4nLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6W1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBicmVha3BvaW50Ojk5MSxcclxuICAgICAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDUsXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6NzY3LFxyXG4gICAgICAgICAgICAgICAgc2V0dGluZ3M6IFwidW5zbGlja1wiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICB9KTtcclxuICAgIFxyXG59XHJcbmZ1bmN0aW9uIHNsaWNrYmxvY2tyZXYxMygpe1xyXG4gICAgJCgnLnNsaWNrYmxvY2tyZXYxMycpLnNsaWNrKHtcclxuICAgICAgICBhcnJvd3M6ZmFsc2UsXHJcbiAgICAgICAgZG90czp0cnVlLFxyXG4gICAgICAgIGluZmluaXRlOmZhbHNlLFxyXG4gICAgICAgIHRvdWNoVGhyZXNob2xkOiA1MCxcclxuICAgICAgICByZXNwb25zaXZlOltcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDo3NjcsXHJcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6MSxcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICB9KTtcclxuICAgIFxyXG59XHJcblxyXG5mdW5jdGlvbiBzbGlja2Jsb2NrcmV2MTQoKXtcclxuICAgICQoJy5zbGlja2Jsb2NrcmV2MTQnKS5zbGljayh7XHJcbiAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxyXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICAgIHRvdWNoVGhyZXNob2xkOiA1MCxcclxuICAgICAgICBkb3RzOiB0cnVlLFxyXG4gICAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgICAgcHJldkFycm93OiAnPGEgY2xhc3M9XCJwcmV2LXNsaWRlXCI+PC9hPicsXHJcbiAgICAgICAgbmV4dEFycm93OiAnPGEgY2xhc3M9XCJuZXh0LXNsaWRlXCI+PC9hPicsXHJcbiAgICAgICAgcmVzcG9uc2l2ZTpcclxuICAgICAgICBbICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBicmVha3BvaW50Ojc2NyxcclxuICAgICAgICAgICAgICAgIHNldHRpbmdzOntcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDIuNSxcclxuICAgICAgICAgICAgICAgICAgICBzd2lwZTp0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYnJlYWtwb2ludDo0ODAsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczp7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLjIsXHJcbiAgICAgICAgICAgICAgICAgICAgc3dpcGU6dHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXVxyXG4gICAgfSk7XHJcbiAgICBcclxufVxyXG5mdW5jdGlvbiBkZXRlY3RGaXJlZm94KCl7XHJcbiAgICBpZigkKCcuc2xpY2tibG9ja3JldjZfd3JhcHBlcicpLmxlbmd0aCAmJiBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZignZmlyZWZveCcpID4gLTEpe1xyXG4gICAgICAgIC8vIERvIEZpcmVmb3gtcmVsYXRlZCBhY3Rpdml0aWVzXHJcbiAgICAgICAgJCgnLnNsaWNrYmxvY2tyZXY2X3dyYXBwZXInKS5maW5kKCdzdmcnKS5yZW1vdmVBdHRyKCdzdHlsZScpO1xyXG4gICAgICAgICQoJy5zbGlja2Jsb2NrcmV2Nl93cmFwcGVyJykuZmluZCgnc3ZnJykuY3NzKHtcclxuICAgICAgICAgICAgcG9zaXRpb246J2Fic29sdXRlJyxcclxuICAgICAgICAgICAgaGVpZ2h0OjAsXHJcbiAgICAgICAgfSlcclxuICAgfVxyXG59XHJcbi8qIGJhY2tQYWdlICovXHJcbmZ1bmN0aW9uIGJhY2tQYWdlKCkge1xyXG4gICAgaWYgKGRvY3VtZW50LnJlZmVycmVyLmluZGV4T2Yod2luZG93LmxvY2F0aW9uLmhvc3QpICE9PSAtMSkge1xyXG4gICAgICAgIHdpbmRvdy5oaXN0b3J5LmJhY2soKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy/guJbguYnguLLguYDguILguYnguLLguYDguJ7guIjguYLguJTguKLguYTguKHguYjguYTguJTguYnguIHguJTguIjguLLguIHguYPguJnguYDguKfguYfguJog4LmD4Lir4LmJIHJlZGlyZWNcclxuICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZSgnLycpO1xyXG4gICAgfVxyXG59XHJcbi8qIGJhY2tQYWdlICovIl0sImZpbGUiOiJzY3JpcHQuanMifQ==
